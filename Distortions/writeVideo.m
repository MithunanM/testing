%% Creates blackand white images

forVid=CT2;
images = cell(size(forVid,3),1);

map=gray(256);
% map=contrast(CT1(:,:,301),256);
% map=[0 0 0; 1 1 1];


 for i=1:size(forVid,3)
    images{i}=cmunique(forVid(:,:,i)); 
 end
 
% create the video writer with 10 fps
 writerObj = VideoWriter('1p5and0p8RegCT2.avi');
 writerObj.FrameRate = 10;

 % open the video writer
 open(writerObj);
 % write the frames to the video
 for u=1:length(images)
     % convert the image to a frame
     frame = im2frame(images{u},map); 
     writeVideo(writerObj, frame);
 end

 close(writerObj);