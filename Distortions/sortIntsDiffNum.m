% Trying to match the number of point on the MRI image to the CT image.

MRImatchedCT=[];
for i=1:size(b,1)

    CTSlice= b{i};
    MRISlice=bfour{i};

    for j=1:size(CTSlice,1)
        
        
        if size(find(MRISlice(:,1)>=CTSlice(j)-5 & MRISlice(:,1)<=CTSlice(j)+5),1)>=1 %See if the point is within an x range on MRI
            found = find(MRISlice(:,1)>=CTSlice(j)-5 & MRISlice(:,1)<=CTSlice(j)+5);
            
            if size(find(MRISlice(found,2)>=CTSlice(j,2)-5 & MRISlice(found,2)<=CTSlice(j,2)+5),1)>=1 %if so see if the point is within an y range on MRI
                MRImatchedCT= [MRImatchedCT; CTSlice(j,:)];
            end
        else
            
        end
    end 

end