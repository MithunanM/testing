tic

load('CT0p8mm');
load('CT4mm'); 

[optimizer,metric]=imregconfig('monomodal');

Rfixed= imref3d(size(CT),1,1,0.8);
Rmoving= imref3d(size(CTf1),1,1,4);
optimizer.MaximumStepLength = 0.03125;

movingRegisteredVolume = imregister(CTf1,Rmoving, CT,Rfixed, 'affine', optimizer, metric);
toc