clear all
close all
clc

% registers 512x512 and 256x256, registers only the gird intersections
%% Registering 512 and 256

[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\ACR_CT');
CT=CT(:,:,132);%isolate grid
CT(CT<1075)=0;%thresh hold
CT(CT>=1075)=1;
CT=mat2gray(CT);
CTholder=CT; 
CC=bwconncomp(CT); % next few lines used to remove circle that outlines grid
numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest,idx] = max(numPixels);
CT(CC.PixelIdxList{idx}) = 0;
CT=CTholder-CT; %remove the identified circle
imshow(CT,[]); title('CT');


Y=dicomread('T1_SE_Y.dcm');
MRIY=Y(:,:,:,5);
MRIY=im2double(MRIY);
MRIY=mat2gray(MRIY);
MRIY2=imcomplement(MRIY);%invert image to make grid bright
MRIY2=imclearborder(MRIY2); %get rid of the white space around phantom
MRIY2(MRIY2<0.1)=0;%threshold for visibility
MRIY2(MRIY2>=0.1)=1;
figure
imshow(MRIY2);title('MRIY')
% MRIY3=imrotate(MRIY2,2);

% Z=dicomread('T1_SE_Z.dcm');
% MRIZ=Z(:,:,:,5);
% MRIZ=im2double(MRIZ);
% MRIZ=mat2gray(MRIZ);
% MRIZ2=imcomplement(MRIZ);%invert image to make grid bright
% MRIZ2=imclearborder(MRIZ2); %get rid of the white space around phantom
% MRIZ2(MRIZ2<0.1)=0;%threshold for visibility
% MRIZ2(MRIZ2>=0.1)=1;
% figure
% imshow(MRIZ2)

X=dicomread('T1_SE_X.dcm');
MRIX=X(:,:,:,5);
MRIX=im2double(MRIX);
MRIX=mat2gray(MRIX);
MRIX2=imcomplement(MRIX);%invert image to make grid bright
MRIX2=imclearborder(MRIX2); %get rid of the white space around phantom
MRIX2(MRIX2<0.1)=0;%threshold for visibility
MRIX2(MRIX2>=0.1)=1;
MRIX2=imrotate(MRIX2,270);
figure
imshow(MRIX2);title('MRIX')

xhold=MRIX2;

%% Registration
% [IregX,BxX,ByX,FxX,FyX] = register_images(MRIX2,CT);
% [IregY,BxY,ByY,FxY,FyY] = register_images(MRIY2,CT);
% 
% figure
% imshow(IregX)
% figure
% imshow(IregY)

%% Get centers and register the grids test
% close all
gridCT=IntLoc(CT);
se=strel('disk', 1);
gridCT=imopen(gridCT,se);
CTcent=findCent(gridCT,4);
imshow(gridCT)

gridY=IntLoc(MRIY2); %grid of unregisterd image
l=bwlabel(gridY);
i=find(l==90);
gridY(i)=0;
% Apply average filter to see the centres more easily using
% h=ones(2,2)/4;
% newIm= imfilter(MRIY2,h);
h=[0 1 0; 1 1 1; 0 1 0]/5;
gridYavg=imfilter(gridY,h);
MRIY2cent=findCent(gridYavg,4);
% MRIY2cent=findCent(gridY,4); % get pixel location of the centers
figure
imshow(gridY)

[IregY,BxY,ByY,FxY,FyY] = register_images(gridY,gridCT);
RegYcent=findCent(IregY,4);

Ind=ones(size(IregY));
regThresh=IregY;
regThresh(regThresh~=0)=1;
Ind=Ind.*regThresh;
DispxY=BxY.*Ind;
DispyY=ByY.*Ind;

figure
plot(MRIY2cent(:,1), MRIY2cent(:,2), 'ro')
hold on
plot(CTcent(:,1), CTcent(:,2), 'b*')
hold off

figure
imagesc(DispxY); title('DispxY')
figure
imagesc(DispyY); title('DispyY')

figure
imshow(cbimage(IregY,gridCT))
%% difference in center values

% col1=0;
% col2=0;
% col3=0;