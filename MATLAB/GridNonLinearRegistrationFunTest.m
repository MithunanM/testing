clear 
close all
clc
tic
% [metaD1,CT1]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\Empty');
% [metaD2,CT2]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\ST1.5');

[metaD1,CT1]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\ST0.8');
[metaD2,CT2]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\MimRegistration\1.5mmTo0.8mm\2016-11__Studies\CT1.5mmTo0.8mmReExport');

%% This whole thing is for Empty
% % for i=1:size(CT1,3)
% % %     CT1(:,:,i)=imsharpen(CT1(:,:,i));
% % %     CT1(:,:,i)=imadjust(CT1(:,:,i));
% % %     CT1(:,:,i)=histeq(CT1(:,:,i));
% % end
% % CT1=reshape(CT1,512,[]);
% % CT1=imadjust(CT1);
% % CT1=reshape(CT1, 512, 512, []);
% CT1(:,:,191:end)=[];
% CT1(:,:,1:5)=[];
% % CT1(1:131,:,:)=0;
% % CT1(381:end,:,:)=0;
% % CT1(:,1:68,:)=0;
% % CT1(:,434:end,:)=0;
% 
% CT1(381:end,:,:)=[];
% CT1(1:131,:,:)=[];
% CT1(:,434:end,:)=[];
% CT1(:,1:68,:)=[];
% 
% CT1(:,:,1:2:end)=[];
%% 0.8 mm

CT1(:,:,664:end)=[]; % for 0.8 mm no tank face
CT1(:,:,1:32)=[];


% CT1(:,:,685:end)=[]; % for 0.8 mm with tank face
% CT1(:,:,1:9)=[];


% CT1(:,419:end,:)=[];
% CT1(:,1:106,:)=[];
% CT1(409:end,:,:)=[];
% CT1(1:195,:,:)=[];


% CT1(:,393:end,:)=[];
% CT1(:,1:110,:)=[];
% CT1(385:end,:,:)=[];
% CT1(1:215,:,:)=[];

% CT1=CT1(:,:,1:2:end);
CT1=CT1(:,:,500:end);

figure
imshow3D(CT1)

%% This whole thing is for 1.5 mm
% CT2=flip(CT2,3);
% CT2=fliplr(CT2);
% 
% % CT2(:,1:95,:)=0;
% % CT2(:,409:end,:)=0;
% % CT2(1:195,:,:)=0;
% % CT2(409:end,:,:)=0;
% 
% CT2(:,409:end,:)=[];
% CT2(:,1:95,:)=[];
% CT2(409:end,:,:)=[];
% CT2(1:195,:,:)=[];
% 
% % for i=1:size(CT2,3)
% %     CT2(:,:,i)=imadjust(CT2(:,:,i));
% % %     CT2(:,:,i)=histeq(CT2(:,:,i));
% % end
% 
% CT2(:,:,1:2:end)=[];

%% 1.5 to 0.8 mm registration  

CT2=flip(CT2,3);
CT2(:,:,672:end)=[]; % for 1.5 mm to 0.8 mm registration no tank face
CT2(:,:,1:33)=[];

% CT2(:,:,697:end)=[]; % for 1.5 mm to 0.8 mm registration with face of tank
% CT2(:,:,1:7)=[];

% CT2(:,419:end,:)=[];
% CT2(:,1:106,:)=[];
% CT2(409:end,:,:)=[];
% CT2(1:195,:,:)=[];

% CT2=CT2(:,:,1:2:end);
CT2=CT2(:,:,500:end);
figure
imshow3D(CT2)


%% Registration

% [optimizer,metric]=imregconfig('monomodal');
% [test]=imregister(CT2,CT1,'affine',optimizer,metric);
% 
% figure
% imshow3D(test)


% 
% [D,Im]=imregdemons(CT2,CT1);
% figure
% imshow3D(Im)

% save('BadD','D')
% save('BadIm','Im')

[Ireg,Bx,By,Bz,Fx,Fy,Fz]=register_volumes(CT1,CT2);

figure
imshow3D(Ireg)

% save('BadBx','Bx')
% save('BadBy','By')
% save('BadBz','Bz')
% save('BadFz','Fz')
% save('BadFy','Fy')
% save('BadFx','Fx')
% save('BadIreg', 'Ireg')

toc