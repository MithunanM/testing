function finalSorted=sortInts(coords)

%% Bad

%% Sorts grid points of every slice
col3= coords(:,3);
change=diff(col3);
steps=find(change>3);
steps=[0 ; steps];%isolate each grid face

finalSorted=zeros(size(coords));
for j=1:size(steps,1)
    if j==size(steps,1) 
        slice=coords(steps(j)+1:end,:);
        sortedSlice=sortrows(slice, [1 2]); %sort in the x and y
        finalSorted(steps(j)+1:end,:)=sortedSlice;
    else
        slice=coords(steps(j)+1:steps(j+1),:);
        sortedSlice=sortrows(slice,[1 2]); %sort in the x and then y
        finalSorted(steps(j)+1:steps(j+1),:)=sortedSlice;
    end
end

end