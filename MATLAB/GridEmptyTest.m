clear all
close all
clc


% NO GRADIENT HERE, uses 3D erotion, then 2D erotion and then intLoc
%% Trying to get intersection of all 3 axis
[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\Empty');

CThold=CT;
CTf=[];
CT(1:155,:,:)=0;
CT(:,1:95,:)=0;
CT(:,400:end,:)=0;
CT(350:end,:,:)=0;
CT=mat2gray(CT);
CT(CT<0.07)=0;
for i=1:size(CT,3) 
    CT(:,:,i)=imsharpen(CT(:,:,i));
end
% CT(CT~=0)=1;
imshow3D(CT);
CT2=CT;

% % h(:,:,1)=[0 0 0; 0 1 0; 0 0 0];
% % h(:,:,2)=[0 1 0; 0 1 0; 0 1 0];
% % h(:,:,3)=[0 0 0; 0 1 0; 0 0 0];


%% Thresholding
CTthresh=[];
tic
for i=1:size(CT,3)  
    CTslice=adaptivethreshold(CT(:,:,i),25,0.01,0);
    CTslice(1:155,:,:)=0;
    CTslice(:,1:95,:)=0;
    CTslice(:,400:end,:)=0;
    CTslice(350:end,:,:)=0;
    CTthresh=cat(3,CTthresh,CTslice);

end
toc


%% Remove the white space caused by the thresholding
h= [1 1 1; 1 1 1; 1 1 1];
tic
blobs=imerode(CTthresh,h);
blobs=imerode(blobs,h);
blobs=imdilate(blobs,h);
blobs=imdilate(blobs,h);
for i=1:size(blobs,3)
    blobs(:,:,i)=bwareaopen(blobs(:,:,i),120);
end
toc
%% Remove the blobs 

CTthresh=CTthresh-blobs;
figure
imshow3D(CTthresh)

%% 2D erosion and Int Loc
% h2=[0 1 0; 1 1 1; 0 1 0];
h2=[0 0 1 0 0; 0 0 1 0 0 ;1 1 1 1 1;0 0 1 0 0; 0 0 1 0 0];
test2=CTthresh;
% test2=imopen(CTthresh,h2);
test2=imerode(CTthresh,h2);
figure
imshow3D(test2)
CTthresh=test2;

% finalIm=[];
% for i=1:size(test2,3)
%     finalImSlice=test2(:,:,i);
%     finalImSlice=IntLoc(finalImSlice);
%     finalImSlice=bwareaopen(finalImSlice,2);
%     finalIm=cat(3,finalIm,finalImSlice);
% end
% 
% % finalIm=imerode(finalIm,h2);
% figure
% % finalIm=imdilate(finalIm,h2);
% % test2=finalIm;
% % for i=1:size(test2,3)
% %     finalIm(:,:,i)=test2(:,:,i);
% %     finalIm(:,:,i)=IntLoc(finalIm(:,:,i));
% %     finalIm(:,:,i)=bwareaopen(finalIm(:,:,i),2);
% % %     finalIm=cat(3,finalIm,finalImSlice);
% % end
% imshow3D(finalIm)


%% 3D erosion and subtraction
h(:,:,1)=[0 0 0; 0 1 0; 0 0 0];
h(:,:,2)=[0 0 0; 0 1 0; 0 0 0];
h(:,:,3)=[0 0 0; 0 1 0; 0 0 0];

test=imerode(CTthresh,h);
CTminusTest=CTthresh-test;
for i=size(CTminusTest,3)
   CTminusTest(:,:,i)=bwareaopen(CTminusTest(:,:,i),2);
end
figure
% % imshow3D(test)
imshow3D(CTminusTest)

% %% 2D erosion and intersection location
% h2=[0 1 0; 1 1 1; 0 1 0];
% test2=imerode(CTminusTest,h2);
% figure
% imshow3D(test2)
% finalIm=[];
% for i=1:size(test2,3)
%     finalImSlice=test2(:,:,i);
%     finalImSlice=IntLoc(finalImSlice);
%     finalIm=cat(3,finalIm,finalImSlice);
% end
% % 
% % % test2=imerode(test2,h2);
% % % test2=imerode(test2,h2);
% figure
% imshow3D(finalIm) %finalIm is the final good image
% % 
% %% Getting coordinates 
% figure
% L=bwlabeln(finalIm,8);
% c=regionprops(L,'centroid');
% coord=vertcat(c.Centroid);
% scatter3(coord(:,1), coord(:,2), coord(:,3),'.')
