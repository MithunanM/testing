% This script is an example of how dicomexport() can be used.
clearvars

%% The map
mapFile = '/Users/stellaxing/Dropbox/Research/LabData/sarcoma_study_mri_data_20141001/Registered data_newmask/Patient18/Pre-treatment/Analysis/T2.mat';%'./exampleDir/mapIAUC.mat';
load(mapFile)
mapData=T2;
% 'iaucDCE' is the map which we wish to export to dicom

%% The template
% We will need a directory that already contains dicom files.
% These dicom files will provide the dicom header.
% These dicom files should be in the same image space as the maps
% Tip: Use the dicom files of the MRI series from which the maps were created

% In this case, I am using a T1-weighted image series for the dicom headers
dcmTemplateDir ='/Users/stellaxing/Dropbox/Research/LabData/sarcoma_study_mri_data_20141001/Registered data_newmask/Patient18/Pre-treatment/Analysis/100';%./exampleDir/T1w';

%% Output settings

% Where should the output dicom files be saved?
outDir = '/Users/stellaxing/Dropbox/Research/LabData/sarcoma_study_mri_data_20141001/Registered data_newmask/Patient18/Pre-treatment/Analysis/T2maps';%./exampleDir';

%%% Additional options for output files

% Give the output a name. Think of this as a variable name (i.e. don't use spaces)
% The output dicom files will be in '$outDir/$Name'
outOpt.Name = 'T2';%'exampleOutput';

% Give a description so people know what the images are showing
outOpt.Description = 'Exported DICOM from T2map';%'Exported DICOM from example.m'; 

% Most dicom viewers will group patients by SeriesID and SeriesNumber.
% So, we need to change these properties of the DICOM header.
% The series number is easy, just pick any large number.
outOpt.SeriesNumber = 1010;
% The SeriesID is a bit more tricky.
% You could just put in any SeriesID, e.g. outOpt.SeriesID = dicomuid;
% which works, but it's not the best way to do things because dicom viewers
% (like Osirix or ARIA) use part of the SeriesID to identify the
% study/patient, and only a small part (the end) describes the current
% series itself.
% A smarter way is to only change this last part so that the remainder of
% the SeriesID is unchanged.
% To do this, use a dot as the first element.
% e.g. '.101' will replace the ending tip of the original SeriesID with 101
outOpt.SeriesID = '.3018';
% If you wish to leave the SeriesID completely unchanged, use:
% outOpt.SeriesID = '';

% The scale factor will multiply the map before exporting it to dicom
% This is done because dicom only supports integers, so if you have a map
% which only has real values from 0 to 1, then the output will only be
% integers 0 and 1. By scaling the map, by lets say a factor of 1000, the
% output dicom will contain those decimal values (0.023 becomes 23 instead of 0).
% In this case, we don't need to scale the map.
% In the case of ADC map, the outOpt.Scale should be 1000000 - Stella
outOpt.Scale = 1;

%% Using dicomexport()

% The setup work is done above. All we need to do now is call the function
dicomexport(mapData,dcmTemplateDir,outDir,outOpt);