% This is an example of using the mimvista software to perform linear
% registration followed by nonlinear

close all
clear all
clc

%% Correct the scaling and rotation of the images

[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\ACR_CT');
CT=CT(:,:,132);%isolate grid
CTorigin=CT;
CTorigin=im2double(CTorigin);
CTorigin=mat2gray(CTorigin);
CT(CT<1075)=0;%thresh hold
CT(CT>=1075)=1;
% CT=CT(1:2:end,:,:); %make CT image same size as MRI
% CT=CT(:,1:2:end,:);
CT=mat2gray(CT);
CTholder=CT; 
CC=bwconncomp(CT); % next few lines used to remove circle that outlines grid
numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest,idx] = max(numPixels);
CT(CC.PixelIdxList{idx}) = 0;
CT=CTholder-CT; %remove the identified circle
% imshow(CT,[]); title('CT');


X=dicomread('T1_SE_X.dcm');
MRIX=X(:,:,:,5);
MRIX=im2double(MRIX);
MRIX=mat2gray(MRIX);
MRIX2=imcomplement(MRIX);%invert image to make grid bright
MRIX2=imclearborder(MRIX2); %get rid of the white space around phantom
MRIX2(MRIX2<0.1)=0;%threshold for visibility
MRIX2(MRIX2>=0.1)=1;
MRIX2=imrotate(MRIX2,270);
% figure
% imshow(MRIX2);title('MRIX')


[metaD,Y]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\Registered_Y');
MRIY=Y(:,:,135);%isolate grid
MRIY=im2double(MRIY);
MRIY=mat2gray(MRIY);
MRIY2=imcomplement(MRIY);
MRIY2=imclearborder(MRIY2);
MRIY2(MRIY2<0.1)=0;%threshold for visibility
MRIY2(MRIY2>=0.1)=1;

[optimizer,metric]=imregconfig('monomodal');
tform=imregtform(MRIY2,CT, 'affine', optimizer,metric);
newY=imwarp(MRIY2,tform);
newY=imresize(newY,[512 512]);


imshow(cbimage(newY,CT));title('Registered mimvista registered again')
% figure
% imshow(MRIY2);title('Registered MRIY')
% figure
% imshow(CT); title('CT')
figure
imshow(cbimage(MRIY2,CT));title('Original mimvista registration') 

%% quick nonlinear registration test
% 
[IregY,BxY,ByY,FxY,FyY] = register_images(newY,CT);
Yind=ones(size(newY));
Yind=Yind.*newY;
DispxY=BxY.*Yind;
DispyY=ByY.*Yind;
figure
imshow(DispxY,[]); title('DispxY')
figure; imshow(DispyY,[]); title('DispyY')

totDispY=(DispxY.^2 + DispyY.^2).^(1/2);
figure;imagesc(BxY); title('BxY')
colormap gray
figure;imagesc(ByY); title('ByY')
colormap gray
figure;imagesc(totDispY); title('total displacement')
colormap gray
figure; imshow(cbimage(IregY,CT)); title('Non-rigid')

