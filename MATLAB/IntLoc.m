%% finds intersections on a grid

function returnIm=IntLoc(Im,s1,s2)

if (nargin==1)
    s1=3;
    s2=3;
end

filtsize=[s1 s2];
g_x=fspecial('gaussian',[1 filtsize(2)]);
g_y=fspecial('gaussian',[filtsize(1) 1]);

filtInX=imfilter(Im,g_x);
fintInX=imfilter(filtInX,g_x);
filtInY=imfilter(Im,g_y);
fintInY=imfilter(filtInX,g_y);

returnIm=filtInX+filtInY;
returnIm(returnIm<1.9)=0; %use for the preregistration images
% returnIm(returnIm<1.7)=0 % test for the postregistration images
end
