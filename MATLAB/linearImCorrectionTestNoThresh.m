close all
clear all
clc

%trying a linear registration with out any thresholding
%% Linear Registration followed by Demons

[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\ACR_CT');
CT=CT(:,:,132);%isolate grid
CT=imsharpen(CT);
CTorigin=CT;
CTorigin=im2double(CTorigin);
CTorigin=mat2gray(CTorigin);
% CT(CT<1075)=0;%thresh hold
% CT(CT>=1075)=1;
% CT=CT(1:2:end,:,:); %make CT image same size as MRI
% CT=CT(:,1:2:end,:);
% CT=mat2gray(CT);
% CTholder=CT; 
% CC=bwconncomp(CT); % next few lines used to remove circle that outlines grid
% numPixels = cellfun(@numel,CC.PixelIdxList);
% [biggest,idx] = max(numPixels);
% CT(CC.PixelIdxList{idx}) = 0;
% CT=CTholder-CT; %remove the identified circle
imshow(CT,[]); title('CT');


X=dicomread('T1_SE_X.dcm');
MRIX=X(:,:,:,5);
MRIX=im2double(MRIX);
MRIX=mat2gray(MRIX);
% MRIX2=imcomplement(MRIX);%invert image to make grid bright
% MRIX2=imclearborder(MRIX2); %get rid of the white space around phantom
% MRIX2(MRIX2<0.1)=0;%threshold for visibility
% MRIX2(MRIX2>=0.1)=1;
MRIX=imrotate(MRIX,270);
figure;imshow(MRIX); title('MRIX');

%% The X registrations 
[optimizer,metric]=imregconfig('multimodal');
[linRegdImX]=imregister(MRIX,CT,'affine',optimizer,metric);
% 
figure;imshow(linRegdImX); title('Linear Regriation');
% figure;imshow(cbimage(linRegdImX,CT)); title('Both images with registration')
% 
% [IregX,BxX,ByX,FxX,FyX]=register_images(linRegdImX,CT);
% % figure;imagesc(BxX); title('BxX')
% % figure;imagesc(ByX); title('ByX')
% 
% Xind=ones(size(linRegdImX));
% Xind=Xind.*linRegdImX;
% DispxX=BxX.*Xind;
% DispyX=ByX.*Xind;
% 
% figure; imagesc(DispxX); title('DispxX'); colormap gray;
% figure; imagesc(DispyX); title('DispyX'); colormap gray;
% totDispX=(DispxX.^2 + DispyX.^2).^(1/2);
% figure; imagesc(totDispX); title('Total Displacement'); colormap gray;

