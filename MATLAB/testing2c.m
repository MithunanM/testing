clc
close all
clear 

% testing finding grid intersetion on a 2D slice
%% working with the CT before grid was built take out commented code using adaptive thresholding
[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\fullGrid\CT');
CThold=CT;
CT=mat2gray(CT(:,:,74));
CT2=CT;
CT(1:155,:,:)=0;
CT(:,1:87,:)=0;
CT(:,420:end,:)=0;
CT(360:end,:,:)=0;
% CT2=imsharpen(CT);
bwim=adaptivethreshold(CT2,19,0.01,0);
bwim(1:155,:,:)=0;
bwim(:,1:87,:)=0;
bwim(:,430:end,:)=0;
bwim(360:end,:,:)=0;
figure
imshow(CT)
figure
imshow(CT2)
figure
imshow(bwim)

h= [ 0 1 0; 1 1 1 ; 0 1 0];
fin=imerode(bwim,h);
fin=mat2gray(fin);
figure
imshow(fin)


t1=IntLoc(fin);
figure
imshow(t1)

% coord=findCent(t1);
% CThold(390:end,:,74)=0;
% 
% figure
% imshow(CThold(:,:,74),[]);
% hold on
% plot(coord(:,1), coord(:,2), 'r.');