close all
clear all
clc

%finds all the intersections because of bwmorph but slightly off
%% finding the grid intersection of the 3D grid phantom
[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\fullGrid\CT');
CThold=CT;
CT=mat2gray(CT);
CTf=[];
for i=1:size(CT,3)
    CTslice=imsharpen(CT(:,:,i));
    CTf=cat(3,CTf,CTslice);
end
CT=CTf;
CT2=CT;
CT(CT<0.05)=0;
CT(CT>0.05)=1;
CT(390:end,:,:)=0;
imshow3D(CT)

%% Grid intersection with thickness of grid reduced 

gridCTthin=[];
h= [ 0 1 0; 1 1 1 ; 0 1 0];

for i=1:size(CT,3);
    CTslice=CT(:,:,i);
%     CTslice=imerode(CTslice,h);
%     gridCTSlice=bwmorph(CT(:,:,i), 'thin'); % thin the thickness of the grid
    gridCTthin=cat(3,gridCTthin,CTslice);
end

figure
imshow3D(gridCTthin)

gridCT=[];
for i=1:size(CT,3)
% for i=30:31
    gridSlice=IntLoc(gridCTthin(:,:,i)); % find the grid intersections 
%     figure
%     imshow(gridSlice);title('test')
    Outer= bwlabel(gridSlice); % Remove outer box
    Outer(Outer~=1)=0;
    gridSlice=gridSlice-2*Outer;
    gridCT=cat(3,gridCT,gridSlice);
end

figure
imshow3D(gridCT)  % Final grid intersections 3D image


%% Grid intersection with red * for morning talk

% t=findCent(gridCT(:,:,50));
% figure
% imshow(CThold(:,:,50),[]);
% hold on
% plot(t(:,1), t(:,2), 'r.');
% hold off

%% MRI images 
% MRI=dicomread('C:\Users\Mithunan\Desktop\fullGrid\MRI\T2w_2D_Tra_3mm_2\IMG-0004-00001');
% MRIfinal=[];
% for i=1:size(MRI,4)
%     MRIslice=MRI(:,:,:,i);
%     MRIfinal=cat(3,MRIfinal,MRIslice);
% end
% MRIfinal=mat2gray(MRIfinal);
% 
% figure
% imshow3D(MRIfinal)

% MRI=mat2gray(MRI);
% MRI(MRI<0.055)=0;
% MRI(MRI>0.055)=1;

% imshow3D(MRI,[])