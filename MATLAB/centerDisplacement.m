clear all
close all
clc

%shows the grid intersections pre and post registration of the whole grid
%% Center coordinated before registration
[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\ACR_CT');

CT=CT(:,:,132);%isolate grid
CToriginal=CT;
CT=mat2gray(CT);
CT=imsharpen(CT);
CT(CT<0.9)=0;%thresh hold
CT(CT>=0.9)=1;
CT=CT(1:2:end,:,:); %make CT image same size as MRI
CT=CT(:,1:2:end,:);

CC=bwconncomp(CT); % next few lines used to remove circle that outlines grid
numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest,idx] = max(numPixels);
CT(CC.PixelIdxList{idx}) = 0;
imshow(CT,[]); title('CT')


Y=dicomread('T1_SE_Y.dcm');
MRIY=Y(:,:,:,5);
MRIY2=thresh(MRIY);
figure
imshow(MRIY2);title('Y');
gridY=IntLoc(MRIY2); %grid of unregistered image
MRIY2cent=findCent(gridY,4); % get pixel location of the centers
figure
imshow(gridY)
title('prereg Y')
hold on
plot(MRIY2cent(:,1), MRIY2cent(:,2), 'b*');
hold off



X=dicomread('T1_SE_X.dcm');
MRIX=X(:,:,:,5);
MRIX2=thresh(MRIX,270);
figure
imshow(MRIX2);title('X');

se=strel('line',2,90);
MRIX2=imclose(MRIX2,se);
se=strel('line',2,0);
MRIX2=imclose(MRIX2,se);
gridX=IntLoc(MRIX2); %grid of unregisterd image
MRIX2cent=findCent(gridX,4); % get pixel location of the centers
figure
imshow(gridX)
title('prereg X')
hold on
plot(MRIX2cent(:,1), MRIX2cent(:,2), 'b*');
hold off

%% Registration

[IregX,BxX,ByX,FxX,FyX] = register_images(MRIX2,CT);
[IregY,BxY,ByY,FxY,FyY] = register_images(MRIY2,CT);

% figure
% imshow(IregX);title('Registered X');
% h=fspecial('gaussian');
% IregX2=imfilter(IregX,h);
% RegGridX=IntLoc(IregX2);
% figure
% imshow(RegGridX)

%% Center coordinates after registration

% IregX(IregX~=0)=1;
% RegGridX= IntLoc(IregX);
% RegXCent=findCent(RegGridX);
% figure
% imshow(RegGridX); title('Registered Image in X');
% hold on
% plot(RegXCent(:,1), RegXCent(:,2), 'b*')
figure
imshow(IregX)
IregX(IregX>.3)=1;
IregX(IregX~=1)=0;
RegGridX=IntLoc(IregX);
RegXCent=findCent(RegGridX);
figure
imshow(RegGridX)
hold on
plot(RegXCent(:,1), RegXCent(:,2), 'b*')

figure
plot(MRIX2cent(:,1), MRIX2cent(:,2), 'b*');
hold on
plot(RegXCent(:,1), RegXCent(:,2), 'r*')
legend('Preregistration centers','Post registration centers')
title('Location of grid intersection on MRIX')



%% Random tests

% close all
% CToriginal=CToriginal(1:2:end,:,:);
% CToriginal=CToriginal(:,1:2:end,:);
% test=movepixels(MRIX, BxX, ByX, []);
% test=mat2gray(test);
% CToriginal=mat2gray(CToriginal);
% t1=cbimage(test,CToriginal);
% close all
% figure
% imshow(t1)
% 
% figure
% imshow(MRIX,[]); title('X')
% hold on
% plot(MRIX2cent(:,1), MRIX2cent(:,2), 'b*')
% 
% figure
% imshow(MRIY,[]); title('Y');
% hold on
% plot(MRIY2cent(:,1), MRIY2cent(:,2), 'b*')
% 
% figure
% imshow(IregX,[]);
% hold on
% plot(RegXCent(:,1), RegXCent(:,2), 'b*')

% figure
% imshow(IregY,[]);
% hold on



