function realFinal=sortIntsFinal(coords,metaD)

%% Good version for 0.8 mm slices only right now (I think it works for 1.5mm too)
% Change value to 1 for 1.5 mm and 3 for 0.8 mm


%% Quick snipet to set how to sort in Z based on slice thickness
a=vertcat(metaD.SliceLocation);

if(a(2)-a(1))<1
    spacer=3;
else
    spacer=1;
end
%% Sorts grid points of every slice in z and brings the similar x axis together
% Isolate each grid, group the x

col3= coords(:,3);
change=diff(col3);
steps=find(change>spacer); % CHANGE THIS FOR DIFFERENT SLICE THICKNESSES
steps=[0 ; steps];%isolate each grid face

diff(steps)

finalSorted=zeros(size(coords));
for j=1:size(steps,1)
    if j==size(steps,1) 
        slice=coords(steps(j)+1:end,:);
        sortedSlice=sortrows(slice, [1 2]); %sort in the x and y
        finalSorted(steps(j)+1:end,:)=sortedSlice;
    else
        slice=coords(steps(j)+1:steps(j+1),:);
        sortedSlice=sortrows(slice,[1 2]); %sort in the x and then y
        finalSorted(steps(j)+1:steps(j+1),:)=sortedSlice;
    end
end

%% Final sort, sort in the y after isolating each x group in the z groups 
col1=finalSorted(:,1);
change=abs(diff(col1));
steps=find(change>3);
steps=[0 ; steps];
realFinal=zeros(size(coords));

for j=1:size(steps,1)
    if j==size(steps,1) 
        slice=finalSorted(steps(j)+1:end,:);
        sortedSlice=sortrows(slice, 2); %sort in the y
        realFinal(steps(j)+1:end,:)=sortedSlice;
    else
        slice=finalSorted(steps(j)+1:steps(j+1),:);
        sortedSlice=sortrows(slice, 2); %sort in the y
        realFinal(steps(j)+1:steps(j+1),:)=sortedSlice;
    end
end

% a=diff(realFinal(:,2));          %Use this to find any extra points that
% a=abs(diff(realFinal(:,2)));     %Shouldnt exist
% b=find(a<11)

end