clear all
close all
clc


%% Testing adaptive thresholding to get all the grid intersections
[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\ACR_CT');

CT=CT(:,:,132);%isolate grid
CTtest=CT;
CT=mat2gray(CT);
CT=imsharpen(CT);
CT(CT<0.9)=0;%thresh hold
CT(CT>=0.9)=1;
ctAT2=CT;
CT=adaptivethreshold(CT,2, 0, 1);

figure
imshow(CT)

se=strel('line',3,90);
CT=imopen(CT,se);
se=strel('line',3,0);
CT=imopen(CT,se);
imshow (CT)
% 
CT=mat2gray(CT);

CC=bwconncomp(CT); % next few lines used to remove circle that outlines grid
numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest,idx] = max(numPixels);
CT(CC.PixelIdxList{idx}) = 0;
figure
% ctAT=ctAT2-ctAT;
imshow(CT,[]); title('CT')

grid=IntLoc(CT,7,7);

figure
imshow(grid)
title('intersection')

%% Y using adaptive
% Y=dicomread('T1_SE_Y.dcm');
% MRIY=Y(:,:,:,5);
% MRIY=imsharpen(MRIY);
% MRIY=mat2gray(MRIY);
% imshow(MRIY,[]); title('MRIY')
% yat=adaptivethreshold(MRIY, 5, 0, 1);
% yat=imcomplement(yat);
% figure
% imshow(yat); title('adaptive')