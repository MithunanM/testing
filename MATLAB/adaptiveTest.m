close all
clear all
clc

% testing apdative thresholding 

X=dicomread('T1_SE_X.dcm');
MRIX=X(:,:,:,5);
MRIX=im2double(MRIX);
MRIX=mat2gray(MRIX);

bwim=adaptivethreshold(MRIX,11,0.03,0);
figure
imshow(bwim)
