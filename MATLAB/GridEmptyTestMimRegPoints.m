clear
close all
clc

% Finding the intersection of the 3 mm to 0.8 mm Mim registration

%% 

[metaDreg,CTreg]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\MimRegistration\1.5mmTo0.8mm\2016-11__Studies\CT1.5mmTo0.8mmReExport');
% [metaDreg,CTreg]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\MimRegistration\2016-11__Studies\3mmTo0.8mm');


CT=CTreg;
CT(:,:,672:end)=[]; % for 1.5 mm to 0.8 mm registration
CT(:,:,1:33)=[];



CT=mat2gray(CT);
CT2=CT;
for i=1:size(CT,3) 
    CT(:,:,i)=imsharpen(CT(:,:,i));
end
CThold2=CT;
CT=reshape(CT,512,[]);
CT=imadjust(CT);
CT=reshape(CT, 512, 512, []);
for i=1:size(CT,3) 
    CT(:,:,i)=imsharpen(CT(:,:,i));
end
CT(CT<0.07)=0;
figure
imshow3D(CT)

CTthresh=zeros(size(CT));

for i=1:size(CT,3)  
    CTslice=adaptivethreshold(CT(:,:,i),20,0.01,0);
    CTthresh(:,:,i)=CTslice;

end


%% Remove the white space caused by the thresholding
h= [1 1 1; 1 1 1; 1 1 1];

blobs=imerode(CTthresh,h);
blobs=imerode(blobs,h);
blobs=imdilate(blobs,h);
blobs=imdilate(blobs,h);
for i=1:size(blobs,3)
    blobs(:,:,i)=bwareaopen(blobs(:,:,i),19);
end


%% Remove the blobs 

CTthresh=CTthresh-blobs;

for i=1:size(CTthresh,3)
    CTthresh(:,:,i)=bwareaopen(CTthresh(:,:,i),20);
end


% % For the 0.8/1.5mm ST 
CTthresh(:,1:110,:)=0;
CTthresh(1:215,:,:)=0;
CTthresh(:,393:end,:)=0;
CTthresh(385:end,:,:)=0;

figure
imshow3D(CTthresh)

%% Finding intersections
se=zeros(7,7);
se(:,4)=1;
se(4,:)=1;


finalIm=zeros(size(CTthresh));

for i=1:size(CT,3)
    finalIm(:,:,i)=imopen(CTthresh(:,:,i),se);
    finalIm(:,:,i)=imopen(finalIm(:,:,i),se);
end

h(:,:,1)=[0 0 0 ;  0 1 0 ; 0 0 0];
h(:,:,2)=[0 0 0 ;  0 1 0 ; 0 0 0];
h(:,:,3)=[0 0 0 ;  0 1 0 ; 0 0 0];
finalImInside=imerode(finalIm,h);
finalIm=finalIm-finalImInside;

figure
imshow3D(finalIm)

test=zeros(size(CTthresh));
for i=1:size(CT,3)
    test(:,:,i)=bwareaopen(finalIm(:,:,i),10);
%     test(:,:,i)=IntLoc(finalIm(:,:,i));
end

for i=1:size(CT,3)
    test(:,:,i)=imopen(test(:,:,i),se);
end

figure
imshow3D(test)

% test(:,:,665:end)=[];
% test(:,:,1:32)=[];

%% location of the intersection
L=bwlabeln(test);
c=regionprops(L,'centroid');
coord1=vertcat(c.Centroid);
figure
scatter3(coord1(:,1), coord1(:,2), coord1(:,3), '.');
xlabel('x'); ylabel('y'); zlabel('z')
rotate3d on

%% Quick sorting of the coordinate points

% coord2=sortIntsFinal(coord1);