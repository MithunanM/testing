close all
clear all
clc

%Linear registers the ACR phantom and then uses the standard matlab demons to get the
%displacement. This still uses manual thresholding.
%% Thresholding the CT and MR images

[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\ACR_CT');
CT=CT(:,:,132);%isolate grid
CTorigin=CT;
CTorigin=im2double(CTorigin);
CTorigin=mat2gray(CTorigin);
CT(CT<1075)=0;%thresh hold
CT(CT>=1075)=1;
% CT=CT(1:2:end,:,:); %make CT image same size as MRI
% CT=CT(:,1:2:end,:);
CT=mat2gray(CT);
CTholder=CT; 
CC=bwconncomp(CT); % next few lines used to remove circle that outlines grid
numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest,idx] = max(numPixels);
CT(CC.PixelIdxList{idx}) = 0;
CT=CTholder-CT; %remove the identified circle
% imshow(CT,[]); title('CT');


X=dicomread('T1_SE_X.dcm');
MRIX=X(:,:,:,5);
MRIX=im2double(MRIX);
MRIX=mat2gray(MRIX);
MRIX2=imcomplement(MRIX);%invert image to make grid bright
MRIX2=imclearborder(MRIX2); %get rid of the white space around phantom
MRIX2(MRIX2<0.1)=0;%threshold for visibility
MRIX2(MRIX2>=0.1)=1;
MRIX2=imrotate(MRIX2,270);
% figure;imshow(MRIX2); title('MRIX');

%% Linear followed by nonlinear registrations
[optimizer,metric]=imregconfig('monomodal');
[linRegdImX]=imregister(MRIX2,CT,'affine',optimizer,metric);

figure;imshow(linRegdImX); title('Linear Registration');
figure;imshow(cbimage(linRegdImX,CT)); title('Both images after linear registration')

% [IregX,BxX,ByX,FxX,FyX]=register_images(linRegdImX,CT);
% figure;imagesc(BxX); title('BxX'); colormap gray;
% figure;imagesc(ByX); title('ByX'); colormap gray;
% 
% figure; imshow(cbimage(IregX,CT)); title('demons kroon cb image')
% figure; imshow(IregX); title('demons kroon')
tic
[D,nonlin]=imregdemons(linRegdImX, CT);
toc

figure
imshow(nonlin);title('demons registration image')
figure; imshow(cbimage(nonlin,CT)); title('nonlinear CB')

Xind=ones(size(linRegdImX));
Xind=Xind.*linRegdImX;
DispxX=D(:,:,1).*Xind;
DispyX=D(:,:,2).*Xind;

figure; imagesc(DispxX); title('Displacement in x of MRIX'); colormap gray;
figure; imagesc(DispyX); title('Displacement in y of MRIX'); colormap gray;


