clear
close all
clc

tic

%Example of why the matlab registration sucks

[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\Empty');
[metaD,CTf1]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\ST0.8');


figure
CT(1:132,:,:)=0;
CT(:,1:69,:)=0;
CT(:,433:end,:)=0;
CT(381:end,:,:)=0;

for i =1:size(CT,3)
  CT(:,:,i)=imsharpen(CT(:,:,i));
end

imshow3D(CT)
CTf1=fliplr(CTf1);
CTf1=flip(CTf1,3);
CTf1(1:194,:,:)=0;
CTf1(:,1:95,:)=0;
CTf1(:,407:end,:)=0;
CTf1(409:end,:,:)=0;

for i =1:size(CTf1,3)
  CTf1(:,:,i)=imsharpen(CTf1(:,:,i));
end

figure
imshow3D(CTf1)




[optimizer,metric]=imregconfig('monomodal');
[test]=imregister(CTf1,CT,'affine',optimizer,metric);

figure
imshow3D(test)

toc
