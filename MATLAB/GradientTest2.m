clear all
close all
clc

[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\fullGrid\CT');
CThold=CT;
CTf=[];
CT(1:155,:,:)=0;
CT(:,1:95,:)=0;
CT(:,420:end,:)=0;
CT(360:end,:,:)=0;
for i=10:size(CT,3)-10
    CTslice=mat2gray(CT(:,:,i));
    CTf=cat(3,CTf,CTslice);
end
CT=CTf;
CT2=CT;
CTthresh=[]; %This is the thresholded CT image
CT(:,:,5:8:end)=0;
CT(:,:,6:8:end)=0;
CT(:,:,7:8:end)=0;
CT(:,:,8:8:end)=0;
imshow3D(CT)

for i=1:size(CT,3)
    CTslice=adaptivethreshold(CTf(:,:,i),19,0.01,0);
    CTslice(1:155,:,:)=0;
    CTslice(:,1:95,:)=0;
    CTslice(:,420:end,:)=0;
    CTslice(360:end,:,:)=0;
    CTthresh=cat(3,CTthresh,CTslice);

end
CTfinal=CTthresh;
CTthresh(:,:,5:8:end)=0;
CTthresh(:,:,6:8:end)=0;
CTthresh(:,:,7:8:end)=0;
CTthresh(:,:,8:8:end)=0;

figure
imshow3D(CTthresh)

[gradx,grady,gradz]=imgradientxyz(CTthresh);
% figure
% imshow3D(abs(gradx))
% figure
% imshow3D(abs(grady))
% figure
% imshow3D(abs(gradz))
% figure
% imshow3D(gradz)
summer=[];
for i=1:size(gradz,3)
    slice=abs(gradz(:,:,i));
    summer(i)=sum(slice(:));
end

summer(summer<500000)=0;
summer(summer>500000)=1;

test=find(summer);

gridPoints=[];
for i=1:size(CT,3)
    
    if sum(test==i)==1
        CTslice=CTthresh(:,:,i);
        h= [ 0 1 0; 1 1 1 ; 0 1 0];
        CTslice=imerode(CTslice,h);
        CTslice=mat2gray(CTslice); % by here you have a skinny grid
        CTslice=IntLoc(CTslice); %intersection
        gridPoints=cat(3,gridPoints,CTslice);
    else
        gridPoints=cat(3,gridPoints,zeros(512,512));
    end
end

figure
imshow3D(gridPoints)

figure
L=bwlabeln(gridPoints,8);
c=regionprops(L,'centroid');
coord=vertcat(c.Centroid);
scatter3(coord(:,1), coord(:,2), coord(:,3),'.')