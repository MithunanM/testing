clear
close all
clc

tic
% Finding the intersection of the 1.5 mm to 0.8 mm Mim registration

%% 

[metaDreg,CTreg]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\MimRegistration\1.5mmTo0.8mm\2016-11__Studies\CT1.5mmTo0.8mmReExport');
% [metaDreg,CTreg]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\MimRegistration\2016-11__Studies\3mmTo0.8mm');


CT=CTreg;
CT=flip(CT,3);
CT(:,:,672:end)=[]; % for 1.5 mm to 0.8 mm registration
CT(:,:,1:33)=[];

CT=mat2gray(CT);
CT2=CT;
for i=1:size(CT,3) 
    CT(:,:,i)=imsharpen(CT(:,:,i));
end
CThold2=CT;
CT=reshape(CT,512,[]);
CT=imadjust(CT);
CT=reshape(CT, 512, 512, []);
for i=1:size(CT,3) 
    CT(:,:,i)=imsharpen(CT(:,:,i));
end
CT(CT<0.07)=0;
figure
imshow3D(CT)

CTthresh=zeros(size(CT));

for i=1:size(CT,3)  
    CTslice=adaptivethreshold(CT(:,:,i),20,0.01,0);
    CTthresh(:,:,i)=CTslice;

end
figure
imshow3D(CTthresh)

%% Remove the white space caused by the thresholding
h= [1 1 1; 1 1 1; 1 1 1];

blobs=imerode(CTthresh,h);
blobs=imerode(blobs,h);
blobs=imdilate(blobs,h);
blobs=imdilate(blobs,h);
for i=1:size(blobs,3)
    blobs(:,:,i)=bwareaopen(blobs(:,:,i),19);
end


%% Remove the blobs 

CTthresh=CTthresh-blobs;

for i=1:size(CTthresh,3)
    CTthresh(:,:,i)=bwareaopen(CTthresh(:,:,i),20);
end


% % For the 0.8/1.5mm ST 
CTthresh(:,1:110,:)=0;
CTthresh(1:215,:,:)=0;
CTthresh(:,393:end,:)=0;
CTthresh(385:end,:,:)=0;

figure
imshow3D(CTthresh)

streak=ones(4,4);
CTthresh2=zeros(size(CTthresh));
for i=1:size(CTthresh,3)
    CTthresh2(:,:,i)=imopen(CTthresh(:,:,i),streak);
end
CTthresh=CTthresh-CTthresh2;
% CTthresh=flip(CTthresh,3);
figure
imshow3D(CTthresh)



%% Finding intersections
se=zeros(7,7);
se(:,4)=1;
se(4,:)=1;


finalIm=zeros(size(CTthresh));

for i=1:size(CT,3)
    finalIm(:,:,i)=imopen(CTthresh(:,:,i),se);
    finalIm(:,:,i)=imopen(finalIm(:,:,i),se);
end

h(:,:,1)=[0 0 0 ;  0 1 0 ; 0 0 0];
h(:,:,2)=[0 0 0 ;  0 1 0 ; 0 0 0];
h(:,:,3)=[0 0 0 ;  0 1 0 ; 0 0 0];
% 
% clear h
% h(:,:,1)=[0 0 0 0 ;  0 1 1 0 ; 0 1 1 0; 0 0 0 0];
% h(:,:,2)=[0 0 0 0 ;  0 1 1 0 ; 0 1 1 0; 0 0 0 0];
% h(:,:,3)=[0 0 0 0 ;  0 1 1 0 ; 0 1 1 0; 0 0 0 0];
finalImInside=imerode(finalIm,h);
finalIm=finalIm-finalImInside;


% se2=ones(2,2);
% finalIm=imdilate(finalIm,se2);
% figure
% imshow3D(finalIm)
% 
% 
test=zeros(size(CTthresh));
for i=1:size(CT,3)
    test(:,:,i)=bwareaopen(finalIm(:,:,i),10);
%     test(:,:,i)=imopen(test(:,:,i),se);
%     test(:,:,i)=IntLoc(test(:,:,i)); %look at slices 314 and 314 to see if its working 
end
for i=1:size(test,3)
   test(:,:,i)=imopen(test(:,:,i),se); 
end

figure
imshow3D(finalIm)


% for i = 1:size(test,3)
%     test(:,:,i)=bwmorph(test(:,:,i), 'shrink', Inf );
%     test(:,:,i)=imdilate(test(:,:,i),ones(3,3));
% end

figure
imshow3D(test)
% % 
% % for i=1:size(CT,3)
% %     test(:,:,i)=imopen(test(:,:,i),se);
% % end
% 
% figure
% imshow3D(test)
% 
% % test(:,:,665:end)=[];
% % test(:,:,1:32)=[];
% 
%% location of the intersection
% L=bwlabeln(test);
% c=regionprops(L,'centroid');
% coord1=vertcat(c.Centroid);
% figure
% scatter3(coord1(:,1), coord1(:,2), coord1(:,3), '.');
% xlabel('x'); ylabel('y'); zlabel('z')
% rotate3d on

%% location of the intersection 2
coord1=[];
for i=1:size(test,3)

    L=bwlabel(test(:,:,i));
    c=regionprops(L,'centroid');
    a=vertcat(c.Centroid);
    b=(ones(size(c))*i);
    coordHolder=horzcat(a,b);
    coord1=vertcat(coord1,coordHolder);

end

figure
scatter3(coord1(:,1), coord1(:,2), coord1(:,3), '.');
xlabel('x'); ylabel('y'); zlabel('z')
rotate3d on


%% Quick sorting of the coordinate points
coord2=sortIntsFinalForRegi(coord1);
coord2(18468,:)=[]; % TAKE THIS OUT FOR THE 1.5 mm to 0.8 mm REGISTRATION ONLY



% coord2(2045,:)=[]; %this was for the registration image but in the wrong direction

toc
% points=coord2(967:967+322,1:2);
% imHold=test(:,:,28);
% figure
% imshow(imHold)
% hold on
% scatter(points(:,1),points(:,2))
