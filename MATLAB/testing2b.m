clc
close all
clear 

% find 2D grid intersection using manual threshhold 
%% working with the CT before grid was built take out commented code
[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\fullGrid\CT');
CThold=CT;
CT=mat2gray(CT(:,:,74));
CT2=CT;
CT(1:150,:,:)=0;
CT(:,422:end,:)=0;
CT(360:end,:,:)=0;
imhist(CT)
CT(CT<0.28)=0;
CT(CT>0.28)=1;

figure
imshow(CT)

t1=IntLoc(CT);
figure
imshow(t1)

lab=bwlabel(t1);
lab(lab==1 | lab==362)=0;
lab(lab~=0)=1;
figure
imshow(lab)

coord=findCent(lab,4);
CThold(390:end,:,74)=0;

figure
imshow(CThold(:,:,74),[]);
hold on
plot(coord(:,1), coord(:,2), 'r.');