%% Loading
clear 
load('0p8metaD.mat')
load('1p5to0p8metaD.mat')
load('grid0p8.mat')
load('grid1p5to0p8RegiOriented.mat')

% coord2=sortIntsFinal(coord1);
%Getting the meta data for the location in Z
scan1=vertcat(metaD1.SliceLocation);
scan2=vertcat(metaD2.SliceLocation);

%% adjust metadata to be same as the slices taken out
scan1(664:end,:)=[]; % for 0.8 mm
scan1(1:32)=[];
scan1=flipud(scan1); % need flip to match original image

% 
% scan2(359:end,:)=[]; % For 1.5mm
% scan2(1:15,:)=[];
% scan2=flipud(scan2); %need flip to match original image
% % scan2=scan2-3;


scan2(672:end,:)=[]; % for 1.5 mm to 0.8 mm registration
scan2(1:33)=[];
scan2=flipud(scan2);

%% Finding how constiant grind intersection points are 
% xsorted=sortrows(coord2, 1);
% lowest=xsorted(1:896:end,1);
% errorsX=zeros(size(xsorted,1),1);
% for i=1:size(xsorted,1)
%     errorsX(i)=xsorted(i,1)-lowest(ceil(i/896));
% end
% maxErrorsX=errorsX(896:896:end);
% 
% 
% ysorted=sortrows(coord2, 2);
% lowest=ysorted(1:1472:end,2);
% errorsY=zeros(size(ysorted,1),1);
% for i=1:size(ysorted,1)
%     errorsY(i)=ysorted(i,2)-lowest(ceil(i/1472));
% end
% maxErrorsY=errorsY(1472:1472:end);

%% Labeling of axis
% origin=coord2(10465,:);
% 
% test(:,1)=(coord2(:,1)-origin(1))*1.3652;
% test(:,2)=(coord2(:,2)-origin(2))*1.3652;
% test(:,3)=(coord2(:,3)-origin(3))*0.8;
% 
% scatter3(test(:,1), test(:,2), test(:,3));
% xlabel('x')
% ylabel('y')
% zlabel('z')
% 
% rotate3d on

%% Comaprison of 1.5 mm and 0.8mm grid points in x and y
%For 0.8mm

% % load('grid1p5mmEdited.mat');
% % load('grid0p8.mat');
% coord1p5mmEdited=grid3mmTest;
% coord1p5mmEdited=coord1p5mmShifted;
% 
% origin08= coord1(10465,:);
% origin3 = coord1p5mmEdited(10465,:);
% 
% % One of the faces doesnt sort properly for now - tested on the 0.8mm and
% % 1.5mm ST
% 
% coord1(10627:10948,:)=[];
% coord1(152,:)=[];
% coord1p5mmEdited(10627:10948,:)=[];
% coord1p5mmEdited(152,:)=[];
% 
% 
% test08(:,1)=(coord1(:,1)-origin08(1))*1.3652;
% test08(:,2)=(coord1(:,2)-origin08(2))*1.3652;
% test08(:,3)=(coord1(:,3)-origin08(3))*0.8;
% 
% 
% test3(:,1)=(coord1p5mmEdited(:,1)-origin3(1))*1.3652;
% test3(:,2)=(coord1p5mmEdited(:,2)-origin3(2))*1.3652;
% test3(:,3)=(coord1p5mmEdited(:,3)-origin3(3))*1.5;
% 
% % test3(:,1)=(coord1p5mmEdited(:,1)-origin3(1))*1.3652;
% % test3(:,2)=(coord1p5mmEdited(:,2)-origin3(2))*1.3652;
% % test3(:,3)=(coord1p5mmEdited(:,3)-origin3(3))*3;
% 
% DisplacementField= abs(test3-test08);
% totalValues=sqrt((DisplacementField(:,1).^2+DisplacementField(:,2).^2));
% 
% figure
% % % hist(totalValues)
% % a=find(totalValues==0);
% % b=find(totalValues>0.5 & totalValues<1);
% % c=find(totalValues>1 & totalValues<2);
% % d=find(totalValues>2 & totalValues<5);
% final=totalValues;
% % final(final>5)=[];
% % a=histogram(final,4);
% hist(final)
% % line([0 0.1024],12947);
% text(0.0512,13200, '12947')
% text(0.6656,6200, '6001')
% text(0.8708,1500, '1241')
% text(1.2788,270, '70')
% text(1.4888,218, '18')
% text(1.8988,208, '2')
% xlabel('Displacement (mm)')
% ylabel('Number of intesection points')
% [w1,w2]=hist(final);
% figure
% showingDisp=cat(2,coord1,totalValues);
% scatter3(showingDisp(:,1),showingDisp(:,2),showingDisp(:,3),ones(size(showingDisp,1), 1)*10,showingDisp(:,4));
% xlabel('X'); ylabel('Y'); zlabel('Z')
% colorbar
% rotate3d on

%% When you include Z and using the registration images 

%doubleclick grid0p8 and grid1p5mmShifted for the points (or grid1p5to0p8Regi)

coord1p5mmEdited=coord2; %the registration/the 1.5mm
% coord1p5mmEdited=coord1p5mmShifted;
% coord1p5mmEdited=coord3to0p8;

origin08= coord1(10465,:);
origin3 = coord1p5mmEdited(10465,:);

% One of the faces doesnt sort properly for now - tested on the 0.8mm and
% 1.5mm ST

coord1(10627:10948,:)=[];
coord1(152,:)=[];
coord1p5mmEdited(10627:10948,:)=[];
coord1p5mmEdited(152,:)=[];

% Another error in faces sorting in the 1.5mm to 0.8mm registration
% coord1(14168:14490,:)=[];
% coord1p5mmEdited(14168:14490,:)=[];
%Below is the better version
% coord1(6119:6762,:)=[];
% coord1p5mmEdited(6119:6762,:)=[];


test08(:,1)=(coord1(:,1)-origin08(1))*1.3652;
test08(:,2)=(coord1(:,2)-origin08(2))*1.3652;
% test08(:,3)=(coord1(:,3)-origin08(3))*0.8;
test08(:,3)=scan1(coord1(:,3));

test3(:,1)=(coord1p5mmEdited(:,1)-origin3(1))*1.3652; %this is regi points
test3(:,2)=(coord1p5mmEdited(:,2)-origin3(2))*1.3652;
% test3(:,3)=(coord1p5mmEdited(:,3)-origin3(3))*0.8;
test3(:,3)= scan2(coord1p5mmEdited(:,3));


% test3(:,1)=(coord1p5mmEdited(:,1)-origin3(1))*1.3652; %this shouldve been named test1.5 really
% test3(:,2)=(coord1p5mmEdited(:,2)-origin3(2))*1.3652;
% test3(:,3)=(coord1p5mmEdited(:,3)-origin3(3))*1.5;

% test3(:,1)=(coord1p5mmEdited(:,1)-origin3(1))*1.3652;
% test3(:,2)=(coord1p5mmEdited(:,2)-origin3(2))*1.3652;
% test3(:,3)=(coord1p5mmEdited(:,3)-origin3(3))*3;

DisplacementField= abs(test3-test08);
% totalValues=sqrt((DisplacementField(:,1).^2+DisplacementField(:,2).^2));

% totalValues=sqrt((DisplacementField(:,1).^2+DisplacementField(:,2).^2) +DisplacementField(:,3).^2);
totalValues=sqrt(DisplacementField(:,3).^2);

figure
% % hist(totalValues)
% a=find(totalValues==0);
% b=find(totalValues>0.5 & totalValues<1);
% c=find(totalValues>1 & totalValues<2);
% d=find(totalValues>2 & totalValues<5);
final=totalValues;
% final(final>5)=[];
% a=histogram(final,4);
hist(final)
% line([0 0.1024],12947);
% text(0.0512,13200, '12947')
% text(0.6656,6200, '6001')
% text(0.8708,1500, '1241')
% text(1.2788,270, '70')
% text(1.4888,218, '18')
% text(1.8988,208, '2')
xlabel('Displacement (mm)')
ylabel('Number of intesection points')
[w1,w2]=hist(final);
figure
showingDisp=cat(2,coord1,totalValues);
scatter3(showingDisp(:,1),showingDisp(:,2),showingDisp(:,3),ones(size(showingDisp,1), 1)*10,showingDisp(:,4));
xlabel('X'); ylabel('Y'); zlabel('Z')
colorbar
rotate3d on
