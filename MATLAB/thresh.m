function returnIm=thresh(Im,deg)

%% Threshold the MRI images

if (nargin==1)
    deg=0;
end
    
Im=im2double(Im);
Im=mat2gray(Im);
returnIm=imcomplement(Im);%invert image to make grid bright
returnIm=imclearborder(returnIm); %get rid of the white space around phantom
returnIm=imsharpen(returnIm);
t=graythresh(returnIm);
returnIm(returnIm<t)=0;
returnIm(returnIm~=0)=1;
returnIm=imrotate(returnIm,deg);

end
