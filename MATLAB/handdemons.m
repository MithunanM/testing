close all


%% imregdemons
% fixed  = imread('hands1.jpg');
% moving = imread('hands2.jpg');
moving=im2double(imread('images/lenag1.png'));  
fixed=im2double(imread('images/lenag2.png')); 
% fixed  = rgb2gray(fixed);
% moving = rgb2gray(moving);

moving = imhistmatch(moving,fixed);

[d,movingReg] = imregdemons(moving,fixed,[500 400 200],...
    'AccumulatedFieldSmoothing',1.3);

% imshow(movingReg)
transx=d(:,:,1);
transy=d(:,:,2);
imshow(movepixels(moving,transy,transx),[]);
figure
%% multimodality

[IregX,BxX,ByX,FxX,FyX] = register_images(moving,fixed);

% subplot(1,3,1), imshow(fixed,[]); title('fixed');
% subplot(1,3,2), imshow(moving,[]); title('moving');
% subplot(1,3,3), imshow(IregX,[]); title('registered');
% figure
% imshow(IregX,[])
imshow(movepixels(moving,BxX,ByX),[])

% figure
% subplot(2,2,1), imagesc(BxX); title('BxX');
% subplot(2,2,2), imagesc(ByX); title('ByX');
% subplot(2,2,3), imagesc(transy); title('transy=BxX');
% subplot(2,2,4), imagesc(transx); title('transx=ByX');


figure
imagesc(BxX-transy); title('BxX-transy');
colormap gray
figure
imagesc(ByX-transx); title('ByX-transx');
colormap gray
figure
imagesc(BxX)
colormap gray
figure
imagesc(transy)
colormap gray