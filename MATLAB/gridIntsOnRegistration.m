tic

close all
clear all
clc

%Finding the intersection points on the registration image of the 3 mm to
%0.8 mm. Doesnt work too well

% [metaD,CT]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\ST0.8');
[metaDreg,CTreg]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\MimRegistration\2016-11__Studies\3mmTo0.8mm');
CT=CTreg;
%% Clean up
CT=mat2gray(CT);
CT2=CT;
for i=1:size(CT,3) 
    CT(:,:,i)=imsharpen(CT(:,:,i));
end
CThold2=CT;
CT=reshape(CT,512,[]);
CT=imadjust(CT);
CT=reshape(CT, 512, 512, []);
for i=1:size(CT,3) 
    CT(:,:,i)=imsharpen(CT(:,:,i));
end
CT(CT<0.07)=0;
figure
imshow3D(CT)

CT(:,:,672:end)=[];
CT(:,:,1:26)=[];

%% Thresholding
CTthresh=zeros(size(CT));

for i=1:size(CT,3)  
    CTslice=adaptivethreshold(CT(:,:,i),20,0.01,0);
    CTthresh(:,:,i)=CTslice;

end

%% Remove the white space caused by the thresholding
h= [1 1 1; 1 1 1; 1 1 1];

blobs=imerode(CTthresh,h);
blobs=imerode(blobs,h);
blobs=imdilate(blobs,h);
blobs=imdilate(blobs,h);
for i=1:size(blobs,3)
    blobs(:,:,i)=bwareaopen(blobs(:,:,i),19);
end


%% Remove the blobs 

CTthresh=CTthresh-blobs;

for i=1:size(CTthresh,3)
    CTthresh(:,:,i)=bwareaopen(CTthresh(:,:,i),20);
end

CTthresh(:,1:110,:)=0;
CTthresh(1:215,:,:)=0;
CTthresh(:,393:end,:)=0;
CTthresh(385:end,:,:)=0;
CTthresh2=CTthresh;
figure
imshow3D(CTthresh2)






%% Finding Intersections

se=zeros(9,9);
se(:,5)=1;
se(5,:)=1;
% 
% 
% h1=[1 1 1; 1 1 1; 1 1 1 ];
% CTthresh=imdilate(CTthresh,h1);
% 
h1=[0 1 0; 1 1 1; 0 1 0 ];
% CTthresh=imerode(CTthresh,h1);

CTthresh(CTthresh~=0)=1;

figure
imshow3D(CTthresh)
% 
% 
% 
% % figure
% % imshow3D(CTthresh)
% 
finalIm=zeros(size(CTthresh));

for i=1:size(CT,3)
    finalIm(:,:,i)=imopen(CTthresh(:,:,i),se);
end

% finalIm=CTthresh; %% TEST LINE
% 
h(:,:,1)=[0 0 0 ;  0 1 0 ; 0 0 0];
h(:,:,2)=[0 0 0 ;  0 1 0 ; 0 0 0];
h(:,:,3)=[0 0 0 ;  0 1 0 ; 0 0 0];
finalImInside=imerode(finalIm,h);
finalIm=finalIm-finalImInside;
% % % test=finalIm;
figure
imshow3D(finalIm)
% 
% 
% 
test=zeros(size(CTthresh));
for i=1:size(CT,3)
    test(:,:,i)=bwareaopen(finalIm(:,:,i),9);
    test(:,:,i)=IntLoc(test(:,:,i));
end

test=imdilate(test,h1);

% 
% % se2=ones(3,3);
% % test=imopen(test,se2);
% 
% 
figure
imshow3D(test)
% 
%% location of the intersection
L=bwlabeln(test);
c=regionprops(L,'centroid');
coord1=vertcat(c.Centroid);
figure
scatter3(coord1(:,1), coord1(:,2), coord1(:,3), '.');



toc