clc
close all
clear 


%% working with the CT before grid was built
[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\fullGrid\CT');
CThold=CT;
CT=mat2gray(CT(:,:,74));
% CTf=[];
% for i=74:74
%     CTslice=imsharpen(CT(:,:,i));
%     CTf=cat(3,CTf,CTslice);
% end
% CT=CTf;
CT2=CT;
CT(360:end,:,:)=0;
imhist(CT)
CT(CT<0.28)=0;
CT(CT>0.28)=1;
% CT(390:end,:,:)=0;
% h=[0 1 0; 1 1 1;0 1 0];
% CT=imerode(CT,h);
% h=[1 1 ; 1 1 ];
% CT=imerode(CT,h);
% CT=imerode(CT,h);
figure
imshow(CT)
% CT=bwmorph(CT,'thin');
% figure
% imshow(CT)


t1=IntLoc(CT);
figure
imshow(t1)

% h=[1 1; 1 1];
% t2=imerode(t1,h);
% figure
% imshow(t2)

% h=[0 1 0; 1 1 1; 0 1 0];
% 
% test=imerode(CT,h);
% figure
% imshow(test)
% h=[1 1 ; 1 1];
% test2=imerode(test,h);
% figure
% imshow(test2)


% 
% test3=IntLoc(test);
% figure
% imshow(test3)