clear all
close all
clc

tic
% Find intersection on both sides, works well, use sortIntsFinal to sort points
% Test for both 0.8mm ST and 1.5mm ST, testing to see if I can
% automatically crop the phantom
%% Trying to get intersection of all 3 axis
[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\ST0.8');
% [metaD,CT]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\Empty');
% [metaD,CT]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\ST1.5');
% [metaD,CT]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\MimRegistration\1.5mmTo0.8mm\2016-11__Studies\CT1.5mmTo0.8mmReExport');



% metaD(:,180)=[]; %For 1.5 mm
CThold=CT;
CT=mat2gray(CT);
slicer=sum(sum(CT));
% slicer2=diff(slicer);
% b1=find(slicer2>200000);
% b2=find(slicer2<-200000);


% CT(:,:,359:end)=[]; % For 1.5mm
% CT(:,:,1:15)=[];
% CT(:,:,182)=[];

% CT(:,:,664:end)=[]; % for 0.8 mm
% CT(:,:,1:32)=[];

% CT(:,:,672:end)=[]; % for 1.5 mm to 0.8 mm registration
% CT(:,:,1:33)=[];

% CThold=CT(:,:,156:313);
% CThold=CT(:,:,600:end);
% CT=CThold;

% CT=mat2gray(CT);
% CT2=CT;
% for i=1:size(CT,3) 
%     CT(:,:,i)=imsharpen(CT(:,:,i));
% end
% CThold2=CT;
% CT=reshape(CT,512,[]);
% CT=imadjust(CT);
% CT=reshape(CT, 512, 512, []);
% for i=1:size(CT,3) 
%     CT(:,:,i)=imsharpen(CT(:,:,i));
% end
% CT(CT<0.07)=0;
figure
imshow3D(CT)



% %% Thresholding
% CTthresh=zeros(size(CT));
% 
% for i=1:size(CT,3)  
%     CTslice=adaptivethreshold(CT(:,:,i),20,0.01,0);
%     CTthresh(:,:,i)=CTslice;
% 
% end
% 
% %% Remove the white space caused by the thresholding
% h= [1 1 1; 1 1 1; 1 1 1];
% 
% blobs=imerode(CTthresh,h);
% blobs=imerode(blobs,h);
% blobs=imdilate(blobs,h);
% blobs=imdilate(blobs,h);
% for i=1:size(blobs,3)
%     blobs(:,:,i)=bwareaopen(blobs(:,:,i),19);
% end
% 
% 
% %% Remove the blobs 
% 
% CTthresh=CTthresh-blobs;
% 
% for i=1:size(CTthresh,3)
%     CTthresh(:,:,i)=bwareaopen(CTthresh(:,:,i),20);
% end
% 
% % % For the 0.8/1.5mm ST 
% CTthresh(:,1:110,:)=0;
% CTthresh(1:215,:,:)=0;
% CTthresh(:,393:end,:)=0;
% CTthresh(385:end,:,:)=0;
% 
% % % For 1.5 mm ST
% 
% % CTthresh(:,1:110,:)=[];
% % CTthresh=padarray(CTthresh,[0 55]);
% % CTthresh(:,1:55,:)=0;
% % CTthresh(:,end-55:end,:)=0;
% 
% % % For the 3mm ST
% % CTthresh(1:156,:,:)=0;
% % CTthresh(350:end,:,:)=0;
% % CTthresh(:,1:99,:)=0;
% % CTthresh(:,417:end,:)=0;
% 
% CTthresh2=CTthresh;
% 
% %% Finding Intersections
% 
% h1=[1 1 1; 1 1 1; 1 1 1 ];
% CTthresh=imdilate(CTthresh,h1);
% 
% h1=[0 1 0; 1 1 1; 0 1 0 ];
% CTthresh=imerode(CTthresh,h1);
% 
% figure
% imshow3D(CTthresh)
% 
% se=zeros(9,9);
% se(:,5)=1;
% se(5,:)=1;
% 
% figure
% imshow3D(CTthresh)
% 
% finalIm=zeros(size(CTthresh));
% 
% for i=1:size(CT,3)
%     finalIm(:,:,i)=imopen(CTthresh(:,:,i),se);
% end
% 
% 
% h(:,:,1)=[0 0 0 ;  0 1 0 ; 0 0 0];
% h(:,:,2)=[0 0 0 ;  0 1 0 ; 0 0 0];
% h(:,:,3)=[0 0 0 ;  0 1 0 ; 0 0 0];
% finalImInside=imerode(finalIm,h);
% finalIm=finalIm-finalImInside;
% figure
% imshow3D(finalIm)
% 
% 
% test=zeros(size(CTthresh));
% for i=1:size(CT,3)
%     test(:,:,i)=bwareaopen(finalIm(:,:,i),10);
%     test(:,:,i)=IntLoc(test(:,:,i));
% end
% 
% % test(:,:,184:end)=[];
% % test(:,:,1:14)=[];
% figure
% imshow3D(test)
% 
% 
% 
% %% location of the intersection
% L=bwlabeln(test);
% c=regionprops(L,'centroid');
% coord1=vertcat(c.Centroid);
% figure
% scatter3(coord1(:,1), coord1(:,2), coord1(:,3), '.');
% xlabel('x'); ylabel('y'); zlabel('z')
% rotate3d on
% 
% %% Quick sorting of the coordinate points
% 
% coord2=sortIntsFinal(coord1,metaD);
% coord2(19737,:)=[]; % JUST FOR THE 0.8MM CT TEST CUZ IT HAD AN EXTRA
% 


toc
