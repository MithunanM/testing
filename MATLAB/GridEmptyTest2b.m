clear all
close all
clc


% Find the center of the grid in z, so only half the control points
%% Trying to get intersection of all 3 axis
[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\Empty');

CThold=CT;
CTf=[];
% CT(1:155,:,:)=0;
% CT(:,1:95,:)=0;
% CT(:,400:end,:)=0;
% CT(350:end,:,:)=0;
CT=mat2gray(CT);
% imshow3D(CT)
CT2=CT;
for i=1:size(CT,3) 
    CT(:,:,i)=imsharpen(CT(:,:,i));
end
CT(CT<0.05)=0;
CThold2=CT;
% figure
% imshow3D(CT);


% % h(:,:,1)=[0 0 0; 0 1 0; 0 0 0];
% % h(:,:,2)=[0 1 0; 0 1 0; 0 1 0];
% % h(:,:,3)=[0 0 0; 0 1 0; 0 0 0];


%% Thresholding
CTthresh=zeros(size(CT));
tic
for i=1:size(CT,3)  
    CTslice=adaptivethreshold(CT(:,:,i),20,0.01,0);
%     CTslice(1:155,:,:)=0;
%     CTslice(:,1:95,:)=0;
%     CTslice(:,400:end,:)=0;
%     CTslice(350:end,:,:)=0;
    CTthresh(:,:,i)=CTslice;

end
toc
% figure
% imshow3D(CTthresh)


%% Remove the white space caused by the thresholding
h= [1 1 1; 1 1 1; 1 1 1];
tic
blobs=imerode(CTthresh,h);
blobs=imerode(blobs,h);
blobs=imdilate(blobs,h);
blobs=imdilate(blobs,h);
for i=1:size(blobs,3)
    blobs(:,:,i)=bwareaopen(blobs(:,:,i),120);
end
toc
% figure
% imshow3D(blobs)
%% Remove the blobs 

CTthresh=CTthresh-blobs;
% figure
% imshow3D(CTthresh)
for i=1:size(CTthresh,3)
    CTthresh(:,:,i)=bwareaopen(CTthresh(:,:,i),20);
end
% figure
% imshow3D(CTthresh)


%% 2D erotion for intersection

% h=[0 1 0 ; 1 1 1; 0 1 0];
h=[0 0 1 0 0; 0 0 1 0 0 ;1 1 1 1 1;0 0 1 0 0; 0 0 1 0 0];
CTthresh=imerode(CTthresh,h);
% figure
% imshow3D(CTthresh)
strands=zeros(size(CTthresh));
for i=1:size(CTthresh,3)
    strands(:,:,i)=bwareaopen(CTthresh(:,:,i),10);
end

CTthresh=CTthresh-strands;
CTthresh(:,1:95,:)=0;
CTthresh(1:153,:,:)=0;
CTthresh(:,417:end,:)=0;
CTthresh(350:end,:,:)=0;

CTthresh(:,:,195:end)=[];
CTthresh(:,:,1:14)=[];

CThold2(:,:,195:end)=[];
CThold2(:,:,1:14)=[];

figure
imshow3D(CTthresh)

test=bwareaopen(CTthresh,6,6);

%% Manually adding points

manualAdd=[2 3 13 14 34 35 71 72 108 109 124 125 158 159 160 165 166]; 
for i=1:size(manualAdd,2)
    j=manualAdd(i);
    test(:,:,j)=test(:,:,j)+CTthresh(:,:,j);
end

test(test>1)=1;

test(:,:,11:end)=[];
save('grid1','test')
figure
imshow3D(test)

% figure
% imshow3D(CThold2)

%% Getting centres

% L=bwlabeln(CTthresh,8);
L=bwconncomp(test,6);
c=regionprops(L,'centroid');
coord2=vertcat(c.Centroid);
figure
scatter3(coord2(:,1), coord2(:,2), coord2(:,3), '.');

% %% 2D erosion and Int Loc
% % h2=[0 1 0; 1 1 1; 0 1 0];
% h2=[0 0 1 0 0; 0 0 1 0 0 ;1 1 1 1 1;0 0 1 0 0; 0 0 1 0 0];
% test2=CTthresh;
% % test2=imopen(CTthresh,h2);
% test2=imerode(CTthresh,h2);
% figure
% imshow3D(test2)
% CTthresh=test2;