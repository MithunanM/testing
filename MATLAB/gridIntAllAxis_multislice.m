close all
clear all
clc

%% finding grid intersection in 3D
[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\ACR_CT');
CTfinal=[];
for i=132:140
   CTslice=CT(:,:,i);
   CTslice(CTslice<1075)=0;%thresh hold
   CTslice(CTslice>=1075)=1;   
   CTslice=CTslice(1:2:end,:,:); %make CT image same size as MRI
   CTslice=CTslice(:,1:2:end,:);
   CTslice=mat2gray(CTslice);
   CTholder=CTslice; 
   CC=bwconncomp(CTslice); % next few lines used to remove circle that outlines grid
   numPixels = cellfun(@numel,CC.PixelIdxList);
   [biggest,idx] = max(numPixels);
   CTslice(CC.PixelIdxList{idx}) = 0;
   CTslice=CTholder-CTslice; %remove the identified circle
   CTfinal=cat(3,CTfinal,CTslice);
end

% Y=dicomread('T1_SE_Y.dcm');
% s=size(Y);
% MRIYfinal=[];
% for i=1:s(end)
%     MRIYslice=MRIY(:,:,:,i);
%     MRIYslice=thresh(MRIYslice);
%     MRIYfinal=cat(3,MRIYfinal,MRIYslice);
% end
% 
% X=dicomread('T1_SE_X.dcm');
% s=size(X);
% MRIXfinal=[];
% for i=1:s(end)
%     MRIXslice=MRIX(:,:,:,i);
%     MRIXslice=thresh(MRIXslice);
%     MRIXfinal=cat(3,MRIXfinal,MRIXslice);
% end

%% Get grid intersections from function 

s=size(CTfinal);
gridCT=[];
for i=1:s(end)
    CTslice=CTfinal(:,:,i);
    gridCTslice=IntLoc(CTslice,3,3);
%     se=strel('disk',1);
%     gridCTslice=imopen(gridCTslice,se);
    gridCT=cat(3,gridCT,gridCTslice);
end
figure
L=bwlabeln(gridCT,8);
c=regionprops(L,'centroid');
cent=vertcat(c.Centroid);
scatter3(cent(:,1), cent(:,2), cent(:,3),'*')
title('Centers of the grid on CT image')



