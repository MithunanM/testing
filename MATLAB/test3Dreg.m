close all
clear all
clc

% tic
[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\fullGrid\CT');
CT=mat2gray(CT);
CTf=[];
for i=1:size(CT,3)
    CTslice=imsharpen(CT(:,:,i));
    CTf=cat(3,CTf,CTslice);
end
CT=CTf;
CT2=CT;
CT(CT<0.05)=0;
CT(CT>0.05)=1;
CT(390:end,:,:)=0;
imshow3D(CT)

gridCTSlice=bwmorph(CT(:,:,30), 'thin');
gridCTSlice=double(gridCTSlice);
gridSlice=IntLoc(gridCTSlice(:,:,1)); % find the grid intersections
OuterCT= bwlabel(gridSlice);
OuterCT(OuterCT~=1)=0;

fprintf('Starting warp\n')

T = maketform('affine',[.5 0 0; .5 2 0; 0 0 1]);
CT2=imtransform(CT,T);
gridCTSlice=bwmorph(CT2(:,:,30), 'thin');
gridCTSlice=double(gridCTSlice);
gridSlice=IntLoc(gridCTSlice(:,:,1)); % find the grid intersections
OuterCT2= bwlabel(gridSlice);
OuterCT2(OuterCT2~=1)=0;

% CT=CT(1:2:end,1:2:end,10:5:170);
% CT2=CT2(1:2:end,1:2:end,10:5:170);

% [optimizer,metric]=imregconfig('monomodal');
% [linRegdImX]=imregister(CT2,CT,'affine',optimizer,metric);
% 
% 
% linRegdImX=linRegdImX(:,:,5:29);
% linRegdImX(linRegdImX~=0)=1;
CT=CT(:,:,10:5:170);
CT2=CT2(:,:,10:5:170);

for i=1:2:size(CT,3)
CT(:,:,i)=OuterCT;
end

for i=1:2:size(CT2,3)
CT2(:,:,i)=OuterCT2;
end

figure
imshow3D(CT)

figure
imshow3D(CT2)

% figure;imshow3D(linRegdImX); title('Linear Registration')



% CT(:,:,2:3:end)=0;
% 
% 
% 
[IregX,BxX,ByX,BzX,FxX,FyX,FzX] = register_volumes(CT,CT);
% figure
imshow3D(IregX)
% toc