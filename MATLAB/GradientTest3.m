clear all
close all
clc


% NO GRADIENT HERE, uses 3D erotion, then 2D erotion and then intLoc
%% Trying to get intersection of all 3 axis
[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\fullGrid\CT');
imshow3D(CT);
CThold=CT;
CTf=[];
CT(1:155,:,:)=0;
CT(:,1:95,:)=0;
CT(:,420:end,:)=0;
CT(360:end,:,:)=0;
for i=10:size(CT,3)-10
    CTslice=mat2gray(CT(:,:,i));
    CTf=cat(3,CTf,CTslice);
end
CT=CTf;
CT2=CT;
CTthresh=[]; %This is the thresholded CT image
CT(:,:,5:8:end)=0;
CT(:,:,6:8:end)=0;
CT(:,:,7:8:end)=0;
CT(:,:,8:8:end)=0;
imshow3D(CT)

% h(:,:,1)=[0 0 0; 0 1 0; 0 0 0];
% h(:,:,2)=[0 1 0; 0 1 0; 0 1 0];
% h(:,:,3)=[0 0 0; 0 1 0; 0 0 0];

h(:,:,1)=[0 0 0; 0 1 0; 0 0 0];
h(:,:,2)=[0 0 0; 0 1 0; 0 0 0];
h(:,:,3)=[0 0 0; 0 1 0; 0 0 0];

%% Thresholding
for i=1:size(CT,3)  
    CTslice=adaptivethreshold(CTf(:,:,i),19,0.01,0);
    CTslice(1:155,:,:)=0;
    CTslice(:,1:95,:)=0;
    CTslice(:,420:end,:)=0;
    CTslice(360:end,:,:)=0;
    CTthresh=cat(3,CTthresh,CTslice);

end
CTfinal=CTthresh;
CTthresh(:,:,5:8:end)=0;
CTthresh(:,:,6:8:end)=0;
CTthresh(:,:,7:8:end)=0;
CTthresh(:,:,8:8:end)=0;

figure
imshow3D(CTthresh)

%% 3D erosion and subtraction
test=imerode(CTthresh,h);
CTminusTest=CTthresh-test;
figure
imshow3D(CTminusTest)

%% 2D erosion and intersection location
h2=[0 1 0; 1 1 1; 0 1 0];
test2=imerode(CTminusTest,h2);
finalIm=[];
for i=1:size(test2,3)
    finalImSlice=test2(:,:,i);
    finalImSlice=IntLoc(finalImSlice);
    finalIm=cat(3,finalIm,finalImSlice);
end

% test2=imerode(test2,h2);
% test2=imerode(test2,h2);
figure
imshow3D(finalIm) %finalIm is the final good image

%% Getting coordinates 
figure
L=bwlabeln(finalIm,8);
c=regionprops(L,'centroid');
coord=vertcat(c.Centroid);
scatter3(coord(:,1), coord(:,2), coord(:,3),'.')
