function writeDWIdicom(pathFolder,ADCmap,nameSeries,header)
% writeDWIdicom(pathFolder,ADCmap,nameSeries,header)
%
% pathFolder: Full path of the folder where you want to save the DICOM files
% ADCmap: mat file containing the ADCmap
% header: Full DICOM header template (struct). header(1) is the DICOM
% header template for the first slice of ADCmap. header(2) is the DICOM
% header template for the second slice of ADCmap.

% Martin Valli�res, 2014
%

% Ex: writeDWIdicom(pwd,ADCmap_100_500_800,'ADCmap_100_500_800',header800)

%seriesNumb=header(1).SeriesNumber+1000;
seriesNumb=header(1).SeriesNumber+1000+ceil(1000*rand(1,1));
nSlice=size(ADCmap,3);

for i=1:nSlice
    header(i).SeriesDescription=nameSeries;
    header(i).SeriesNumber=seriesNumb;
    nameSave=['Image',num2str(i),'.dcm'];
    dicomwrite(int16(ADCmap(:,:,i)),nameSave,header(i));
end

end