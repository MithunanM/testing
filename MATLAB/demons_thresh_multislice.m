close all
clear all
clc

%% remove everything but the grid before registration

[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\ACR_CT');
CTfinal=[];
for i=132:140
   CTslice=CT(:,:,i);
   CTslice(CTslice<1075)=0;%thresh hold
   CTslice(CTslice>=1075)=1;   
   CTslice=CTslice(1:2:end,:,:); %make CT image same size as MRI
   CTslice=CTslice(:,1:2:end,:);
   CTslice=mat2gray(CTslice);
   CTholder=CTslice; 
   CC=bwconncomp(CTslice); % next few lines used to remove circle that outlines grid
   numPixels = cellfun(@numel,CC.PixelIdxList);
   [biggest,idx] = max(numPixels);
   CTslice(CC.PixelIdxList{idx}) = 0;
   CTslice=CTholder-CTslice; %remove the identified circle
   CTfinal=cat(3,CTfinal,CTslice);
end


% Y=dicomread('T1_SE_Y.dcm');
s=size(Y);
MRIYfinal=[];
for i=1:s(end)
    MRIXslice=MRIY(:,:,:,i);
    MRIXslice=thresh(MRIXslice);
    MRIYfinal=cat(3,MRIYfinal,MRIXslice);
end

% X=dicomread('T1_SE_X.dcm');
s=size(X);
MRIXfinal=[];
for i=1:s(end)
    MRIxslice=MRIX(:,:,:,i);
    MRIXslice=thresh(MRIXslice);
    MRIYfinal=cat(3,MRIYfinal,MRIXslice);
end

[IregX,BxX,ByX,BzX,FxX,FyX,FzX] = register_volumes(MRIXfinal,CT);
[IregY,BxY,ByY,BzY,FxY,FyY,FzY] = register_volumes(MRIYfinal,CT);


figure
imshow3D(IregX)
figure
imshow3D(IregY)
% figure
% imshow3D(IregX)