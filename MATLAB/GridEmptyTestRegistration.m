clear all
close all
clc

tic
% This is bare bones and doesnt work 

% % Testing registration
% %% First CT image
% [metaD,CT]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\Empty');
% 
% CThold=CT;
% % CT(1:155,:,:)=0;
% % CT(:,1:95,:)=0;
% % CT(:,400:end,:)=0;
% % CT(350:end,:,:)=0;
% CT=mat2gray(CT);
% % imshow3D(CT)
% % CTf=zeros(258,373,221);
% CTf=zeros(size(CT));
% for i=1:size(CT,3)
%     CT(:,:,i)=imsharpen(CT(:,:,i));
%     CTf(:,:,i)=CT(:,:,i);
% %     CTf(:,:,i)=imcrop(CT(:,:,i), [66.51,126.51,372.98,257.98]);
% end
% 
% CTthresh=zeros(size(CTf));
% for i=1:size(CTf,3)  
%     CTslice=adaptivethreshold(CTf(:,:,i),20,0.01,0);
%     CTthresh(:,:,i)=CTslice;
% end
% 
% %% Remove the white space caused by the thresholding
% h= [1 1 1; 1 1 1; 1 1 1];
% 
% blobs=imerode(CTthresh,h);
% blobs=imerode(blobs,h);
% blobs=imdilate(blobs,h);
% blobs=imdilate(blobs,h);
% for i=1:size(blobs,3)
%     blobs(:,:,i)=bwareaopen(blobs(:,:,i),19);
% end
% 
% 
% %% Remove the blobs 
% 
% CTthresh=CTthresh-blobs;
% 
% for i=1:size(CTthresh,3)
%     CTthresh(:,:,i)=bwareaopen(CTthresh(:,:,i),20);
% end
% CTf=CTthresh;
% 
% %% Crop
% CTf1=zeros(258,373,221);
% for i=1:size(CT,3)
%      CTf1(:,:,i)=imcrop(CTf(:,:,i), [66.51,126.51,372.98,257.98]);
% end 
% 
% CTf1(:,:,195:end)=[];
% CTf1(:,:,1:14)=[];
% CTf1(CTf1<0.05)=0;
% % CTf1=CTf;
% figure
% imshow3D(CTf1);

% Second CT image
%% Second CT image
[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\ST0.8');

CThold=CT;
CT=mat2gray(CT);
% CTf=zeros(224,320,694);
CTf=zeros(size(CT));
for i=1:size(CT,3)
    CT(:,:,i)=imsharpen(CT(:,:,i));
    CTf(:,:,i)=CT(:,:,i);
%     CTf(:,:,i)=imcrop(CT(:,:,i), [ 102.5100  189.5100  319.9800  223.9800]);
end

CTthresh=zeros(size(CTf));
for i=1:size(CTf,3)  
    CTslice=adaptivethreshold(CTf(:,:,i),20,0.01,0);
    CTthresh(:,:,i)=CTslice;
end

%% Remove the white space caused by the thresholding
h= [1 1 1; 1 1 1; 1 1 1];

blobs=imerode(CTthresh,h);
blobs=imerode(blobs,h);
blobs=imdilate(blobs,h);
blobs=imdilate(blobs,h);
for i=1:size(blobs,3)
    blobs(:,:,i)=bwareaopen(blobs(:,:,i),19);
end


%% Remove the blobs 
CTthresh=CTthresh-blobs;

for i=1:size(CTthresh,3)
    CTthresh(:,:,i)=bwareaopen(CTthresh(:,:,i),20);
end
CTf=CTthresh;

%% crop
CTf2=zeros(224,320,694);

for i=1:size(CTf,3)
    CTf2(:,:,i)=imcrop(CTf(:,:,i), [102.5100  189.5100  319.9800  223.9800]);
end



CTf2(:,:,667:end)=[];
CTf2(:,:,1:28)=[];
% CTf2(CTf2<0.05)=0;
figure
imshow3D(CTf2);

% [D, Im] = imregdemons(CTf,CTf1);
% [Im,BX,BY,BZ,FX,FY,FZ]=register_volumes(CTf,CTf1);

toc