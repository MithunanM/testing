close all
clear all
clc

%% Correct the scaling and rotation of the images

[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\ACR_CT');
CT=CT(:,:,132);%isolate grid
CTorigin=CT;
CTorigin=im2double(CTorigin);
CTorigin=mat2gray(CTorigin);
CT(CT<1075)=0;%thresh hold
CT(CT>=1075)=1;
% CT=CT(1:2:end,:,:); %make CT image same size as MRI
% CT=CT(:,1:2:end,:);
CT=mat2gray(CT);
CTholder=CT; 
CC=bwconncomp(CT); % next few lines used to remove circle that outlines grid
numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest,idx] = max(numPixels);
CT(CC.PixelIdxList{idx}) = 0;
CT=CTholder-CT; %remove the identified circle
% imshow(CT,[]); title('CT');


X=dicomread('T1_SE_X.dcm');
MRIX=X(:,:,:,5);
MRIX=im2double(MRIX);
MRIX=mat2gray(MRIX);
MRIX2=imcomplement(MRIX);%invert image to make grid bright
MRIX2=imclearborder(MRIX2); %get rid of the white space around phantom
MRIX2(MRIX2<0.1)=0;%threshold for visibility
MRIX2(MRIX2>=0.1)=1;
MRIX2=imrotate(MRIX2,270);
% figure
% imshow(MRIX2);title('MRIX')


% [metaD,Y]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\Registered_Y');
% MRIY=Y(:,:,135);%isolate grid
% MRIY=im2double(MRIY);
% MRIY=mat2gray(MRIY);
% MRIY2=imcomplement(MRIY);
% MRIY2=imclearborder(MRIY2);
% MRIY2(MRIY2<0.1)=0;%threshold for visibility
% MRIY2(MRIY2>=0.1)=1;
% 
% imshow(MRIY2);title('Registered MRIY')
% figure
% imshow(CT); title('CT')
% figure
% imshow(cbimage(MRIY2,CT))

%% quick nonlinear registration test
% 
% [IregY,BxY,ByY,FxY,FyY] = register_images(MRIY2,CT);
% Yind=ones(size(MRIY2));
% Yind=Yind.*MRIY2;
% DispxY=BxY.*Yind;
% DispyY=ByY.*Yind;
% figure
% imshow(DispxY,[]); title('DispxY')
% figure; imshow(DispyY,[]); title('DispyY')


%% 3D similarity or affine correction

% steps=size(X);
% MRIim=[];
% for i=1:steps(4)
%     MRIim=cat(3,MRIim,X(:,:,:,i));
% end

[optimizer,metric]=imregconfig('monomodal');
% optimizer.MaximumIterations=1000;
% optimizer.MaximumStepLength=0.05;
MRIX2(1:51,:)=0;
CT(1:126,:)=0;
% tform=imregtform(MRIX2,CT, 'affine', optimizer, metric);
% newX=imwarp(MRIX2,tform);
% newX(newX>=0.2)=1;
% newX(newX~=1)=0;

% newX2=makeSameSize(newX,CT);
% 
imshow(CT);title('CT')
figure;imshow(newX2); title('MRI')
figure;imshow(cbimage(newX2,CT)); title('both')


[IregY,BxY,ByY,FxY,FyY] = register_images(newX2,CT); %actually X
figure; imshow(IregY); title('New Registration')

% CT90=imrotate(CT,90);
% newX90=imrotate(newX2,90);
% [optimizer,metric]=imregconfig('monomodal');
% tform=imregtform(newX90,CT90, 'affine', optimizer, metric);
% finalX=imwarp(newX90,tform);
% % finalX=imrotate(finalX,-90);
% CTx=makeSameSize(finalX,CT); %use if final larger than CT
% CT90x=makeSameSize(finalX,CT90);
% finalX=makeSameSize(finalX,CT);
%  
% figure
% imshow(finalX);title('MRI')
% figure
% imshow(cbimage(CT90x,finalX)); title('both')

% figure
% imshow3D(X)

%% This is only 2D
% tform=imregcorr(MRIX2,CT);
% newX=imwarp(MRIX2,tform);
% newX=padarray(newX, [35 35]);
% figure
% imshow(cbimage(newX,CT));

%% Random extend image size found online
% nrows = max(size(I1,1), size(I2,1));
% ncols = max(size(I1,2), size(I2,2));
% nchannels = size(I1,3);
% 
% extendedI1 = [ I1, zeros(size(I1,1), ncols-size(I1,2), nchannels); ...
%   zeros(nrows-size(I1,1), ncols, nchannels)];
% 
% extendedI2 = [ I2, zeros(size(I2,1), ncols-size(I2,2), nchannels); ...
%   zeros(nrows-size(I2,1), ncols, nchannels)];
% 
% I1=extendedI1;
% I2=extendedI2;

%% splitting the grid into horizonal and vertical
se=strel('line',2,0);
seCT=strel('line',4,0);
% vertCT=imopen(CT,seCT);
% test=bwlabel(vertCT);
% test(test==5)=0;
% vertCT=test;
% vertMRIX=imopen(MRIX2,se);
% test=bwlabel(vertMRIX);
% test(test==5)=0;
% vertMRIX=test;
% [optimizer,metric]=imregconfig('monomodal');
% tform=imregtform(vertMRIX,vertCT, 'affine', optimizer, metric);
% newvertX=imwarp(vertMRIX,tform);
% tform=imregtform(newvertX,vertCT, 'translation', optimizer, metric);
% newvertX=imwarp(newvertX,tform);
% newvertX=makeSameSize(newvertX,vertCT);
% 
% imshow(newvertX)
% figure
% imshow(vertCT)

% horiCT=imopen(CT,seCT);
% horiMRIX=imopen(MRIX2,se);
% [optimizer,metric]=imregconfig('monomodal');
% tform=imregtform(horiMRIX,horiCT,'affine',optimizer,metric);
% newHoriX=imwarp(horiMRIX,tform);
% figure;imshow(horiCT)
% figure;imshow(horiMRIX)