% function final=sortedInts(coords)
load('coord1.mat');
load('coord2.mat');
coords=coord1;
%% Sorting the coordinates of the intersections
col3= coords(:,3);
change=diff(col3);
steps=find(change>3);
steps=[0 ; steps];
full3D=zeros(size(coords));

for j=1:size(steps,1)
    if j==size(steps,1)
        slice=coords(steps(j)+1:end,:);
        change2=diff(slice(:,1));%starting here sorts the grid intersection of 1 slice
        stepsInside=find(change2>3);
        stepsInside=[0 ; stepsInside];
        finalSteps=zeros(size(slice));
        for i=1:size(stepsInside,1) 
    
            if i==size(stepsInside,1)
                holdSteps=slice(stepsInside(i)+1:end,:); %deal with final set of intersections
                holdSteps=sortrows(holdSteps,2);
                finalSteps(stepsInside(i)+1:end,:)=holdSteps;
            else %sort the intersections
                holdSteps=slice(stepsInside(i)+1:stepsInside(i+1),:);
                holdSteps=sortrows(holdSteps,2);
                finalSteps(stepsInside(i)+1:stepsInside(i+1),:)=holdSteps;
            end
        end
        
        full3D(steps(j)+1:end,:)=finalSteps;
    else
        slice=coords(steps(j)+1:steps(j+1),:);
        change2=diff(slice(:,1));%starting here sorts the grid intersection of 1 slice
        stepsInside=find(change2>3);
        stepsInside=[0 ; stepsInside];
        finalSteps=zeros(size(slice));
        for i=1:size(stepsInside,1) 
    
            if i==size(stepsInside,1)
                holdSteps=slice(stepsInside(i)+1:end,:); %deal with final set of intersections
                holdSteps=sortrows(holdSteps,2);
                finalSteps(stepsInside(i)+1:end,:)=holdSteps;
            else %sort the intersections
                holdSteps=slice(stepsInside(i)+1:stepsInside(i+1),:);
                holdSteps=sortrows(holdSteps,2);
                finalSteps(stepsInside(i)+1:stepsInside(i+1),:)=holdSteps;
            end
        end
        full3D(steps(j)+1:steps(j+1),:)=finalSteps;
    end


end
% newCol=sqrt(slice(:,1).^2+slice(:,2).^2);
% test=cat(2,slice,newCol);
% 
% 
% coords2=coord2;
% %% Sorting the coordinates of the intersections
% col3= coords2(:,3);
% change=diff(col3);
% steps=find(change>3);
% steps=[1 ; steps];
% 
% slice2=coords2(steps(1):steps(2),:);
% newCol2=sqrt(slice2(:,1).^2+slice2(:,2).^2);
% test2=cat(2,slice2,newCol2);
% 
% 
% test=sortrows(test,[1 2 ]);
% test2=sortrows(test2, [1 2 ]);
% a=test2-test;
% 
% max(abs(test2-test))

% figure
% scatter3(slice(:,1), slice(:,2), slice(:,3), '.');
 
% for i=steps(1):steps(2)
%     slice=coords(i)
% end

% end