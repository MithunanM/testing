clear all
close all
clc

%only shows the adaptive thresholding image, use gridInt.m for the manual
%one
%% adaptive thresholding
Y=dicomread('T1_SE_Y.dcm');
MRIY=Y(:,:,:,5);
MRIY=im2double(MRIY);
MRIY=mat2gray(MRIY);
MRIY2=imcomplement(MRIY);%invert image to make grid bright
MRIY2=imclearborder(MRIY2); %get rid of the white space around phantom
MRIhold=MRIY;
MRIY2(MRIY2<0.1)=0;%threshold for visibility
MRIY2(MRIY2>=0.1)=1;
figure
imshow(MRIY2);title('MRIY')



MRIbw=adaptivethreshold(MRIhold,25,0.05,0);
MRIbw=imcomplement(MRIbw);%invert image to make grid bright
MRIbwhold=MRIbw;
CC=bwconncomp(MRIbw); % next few lines used to remove circle that outlines grid
numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest,idx] = max(numPixels);
MRIbw(CC.PixelIdxList{idx}) = 0;
MRIbw=MRIbwhold-MRIbw;
figure
% MRIbw(1:50,:)=0;
imshow(MRIbw)



test=IntLoc(MRIbw);
figure
imshow(test)


final=bwareaopen(test,2);
figure
imshow(final)
lines=bwareaopen(final,8);
final=final-lines;
figure
imshow(final)

test=findCent(final);

figure
imshow(MRIY); title('Adaptive Threshold')
hold on
plot(test(:,1), test(:,2), 'r.');





% MRIbw2=adaptivethreshold(MRIhold,20,0.07,0);
% MRIbw3=adaptivethreshold(MRIhold,20,0.09,0);
% MRIbw4=adaptivethreshold(MRIhold,20,0.11,0);
% MRIbw5=adaptivethreshold(MRIhold,20,0.13,0);
% MRIbw6=adaptivethreshold(MRIhold,20,0.15,0);

% MRIbw2=imcomplement(MRIbw2);
% MRIbw3=imcomplement(MRIbw3);
% MRIbw4=imcomplement(MRIbw4);
% MRIbw5=imcomplement(MRIbw5);
% MRIbw6=imcomplement(MRIbw6);

% figure
% subplot(2,3,1)
% imshow(MRIbw)
% subplot(2,3,2)
% imshow(MRIbw2);title('2')
% subplot(2,3,3)
% imshow(MRIbw3);title('3')
% subplot(2,3,4)
% imshow(MRIbw4);title('4')
% subplot(2,3,5)
% imshow(MRIbw5);title('5')
% subplot(2,3,6)
% imshow(MRIbw6);title('6')
