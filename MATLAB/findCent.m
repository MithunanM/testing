function cent=findCent(im,n)
%using region props to get the center of a pixel cluster

if (nargin==1)
    n=8;
end 

    L=bwlabel(im,n);
    c=regionprops(L,'centroid');
    cent=vertcat(c.Centroid);
end