close all
clear all
clc

%% 2D attempt with imregdemons

X=dicomread('T1_SE_X.dcm');
X(X<500)=1;
X(X>=500)=0;
I1=im2double(X(:,:,:,5));
I1=imrotate(I1,-90);
[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\ACR_CT');
CT=CT(:,:,132);
CT=CT(1:2:end,:,:);
CT=CT(:,1:2:end,:);
I2=im2double(CT);

subplot(1,3,1)
imshow(I1,[])
title('MRI')
subplot(1,3,2)
imshow(I2,[])
title('CT')

[D,moving_reg]=imregdemons(I1,I2,[500 400 200],'AccumulatedFieldSmoothing',1.3);
subplot(1,3,3)
imshow(moving_reg,[])

figure 
x=I1-moving_reg;
imshow(x,[])
