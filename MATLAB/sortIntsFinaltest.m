% function realFinal=sortIntsFinal(coords)

%% Good version

%% Sorts grid points of every slice in z and brings the similar x axis together
% Isolate each grid, group the x
coords=coord1;
col3= coords(:,3);
change=diff(col3);
steps=find(change>3);
steps=[0 ; steps];%isolate each grid face

%diff(steps) %this gives how many points per face  

finalSorted=zeros(size(coords));
for j=1:size(steps,1)
    if j==size(steps,1) 
        slice=coords(steps(j)+1:end,:);
        sortedSlice=sortrows(slice, [1 2]); %sort in the x and y
        finalSorted(steps(j)+1:end,:)=sortedSlice;
    else
        slice=coords(steps(j)+1:steps(j+1),:);
        sortedSlice=sortrows(slice,[1 2]); %sort in the x and then y
        finalSorted(steps(j)+1:steps(j+1),:)=sortedSlice;
    end
end

%% Final sort, sort in the y after isolating each x group in the z groups 
col1=finalSorted(:,1);
change=abs(diff(col1));
steps=find(change>7);
steps=[0 ; steps];
realFinal=zeros(size(coords));

for j=1:size(steps,1)
    if j==size(steps,1) 
        slice=finalSorted(steps(j)+1:end,:);
        sortedSlice=sortrows(slice, 2); %sort in the y
        realFinal(steps(j)+1:end,:)=sortedSlice;
    else
        slice=finalSorted(steps(j)+1:steps(j+1),:);
        sortedSlice=sortrows(slice, 2); %sort in the y
        realFinal(steps(j)+1:steps(j+1),:)=sortedSlice;
    end
end


coord2=realFinal;


%% removing some sorting error for a registered image
% coord2(18354:18997,:,:)=[];
% coord2(15134:15777,:,:)=[];
% coord2(9982:10625,:,:)=[];
% coord2(2045,:)=[];
% a=diff(realFinal(:,2));          %Use this to find any extra points that
% a=abs(diff(coord2(:,3)));     %Shouldnt exist
% b=find(a>1)
% diff(b)




% end