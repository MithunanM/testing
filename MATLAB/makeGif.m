x = 0:0.01:1;
figure(1)
filename = 'adaptiveT.gif';
for n = 1:size(CTthresh,3)
      
      imshow(CTthresh(:,:,n))
%       pause(0.2);
      frame = getframe(1);
      im = frame2im(frame);
      [imind,cm] = rgb2ind(im,256);
      if n == 177;
          imwrite(imind,cm,filename,'gif', 'Loopcount',inf);
      else
          imwrite(imind,cm,filename,'gif','WriteMode','append');
      end
end