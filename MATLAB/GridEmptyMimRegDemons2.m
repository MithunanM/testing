close all
clear all
clc

% CURRENTLY USING NOT A DEMONS
%% Registration following a linear registration
tic

[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\Edited0.8');
[metaDreg,CTreg]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\MimRegistration\2016-11__Studies\3mmTo0.8mm');

CT(:,:,688:end)=[];
CT(:,:,1:8)=[];

CTreg(:,:,695:end)=[];
CTreg(:,:,1:3)=[];

fprintf('Here')
[D,Im]=imregdemons(CTreg,CT);
% [Im,BX,BY,BZ,FX,FY,FZ]=register_volumes(CTreg,CT);
save('Im','Im')
save('BX','BX')
save('BY','BY')
save('BZ','BZ')
toc
