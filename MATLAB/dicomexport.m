function dcmHeader = dicomexport(inData, dcmTemplate, outDir, outOpt)
%% Description:
% This function exports a 2D, 3D or 4D matrix as dicom files.
% Requires an individual dicom header template for each output
% Input:
%       inData - Multidimensional Matrix to be exported (2D, 3D, or 4D)
%       dcmTemplate - Path to directory containing dicom files for header 
%           file who's header will be used.
%       outDir - Directory where output will be exported
%       outOpt - struct containing the (optional) fields:
%               'SeriesID', 'SeriesNumber', 'Name', 'Description', 'Scale'

% Further details on outOpt:
% - 'Name' will be used as the name for the dicom field as well as the name
%   for the subdirectory containing those dicom files. The default name is
%   "dicom"
% - 'Scale' will scale the input data (useful if input is non-integer)
% - 'SeriesID' will replace the "(0020,000E) Series Instance UID" field in the
%   dicom header. If the input is '', then the dicom header's series ID
%   will be remained untouched. If the input starts with a dot (e.g '.01')
%   then this value will be appended at the end of the existing series ID.
% - 'SeriesNumber' will replace the "(0020,0011) Series Number" field in
%   the dicom header. The default series number is 1024.
% - 'Description' will replace the "(008,103E) Series Description" field in
%   the dicom header. The default description is "Exported dicom"

%% Deal with optional outputs

if nargin < 4
    outOpt.Grbg = 1;
end

if ~isfield(outOpt,'SeriesID') outOpt.SeriesID=dicomuid; end
if ~isfield(outOpt,'SeriesNumber') outOpt.SeriesNumber=1024; end
if ~isfield(outOpt,'Name') outOpt.Name='dicom'; end
if ~isfield(outOpt,'Description') outOpt.Description='Exported dicom'; end
if ~isfield(outOpt,'Scale') outOpt.Scale=1; end


outDir = fullfile(outDir,outOpt.Name); % Output is in subdirectory

%% Make sure output directory exists
if ~exist(outDir,'dir')
    mkdir(outDir)
end

%% Pre-process input data

% Convert to uint16 - Needed for signal data
inData = uint16(outOpt.Scale*inData); 

% Melt data as X-by-Y-by-N where N is the number of output images
% (i.e. there will be N dicom files in output)
S = size(inData);
switch length(S)
    case 2
        [sX sY] = size(inData);
        sZ = 1;
        sT = 1;
    case 3
        [sX sY sZ] = size(inData);
        sT = 1;
    case 4
        [sX sY sZ sT] = size(inData);
        inData = reshape(inData,[sX sY sZ*sT]);
end

%% Make sure we have enough dicom files to act as header for output

dcmFiles = dir([dcmTemplate '/*.dcm']);
if length(dcmFiles) ~= (sZ*sT)
    error('Number of dicom files in template directory do not match number of images in input')
    return
end

%% Main loop for exporting

for i=1:length(dcmFiles)
    % Obtain the dicom header
    dcmFile = fullfile(dcmTemplate,dcmFiles(i).name);
    dcmHeader = dicominfo(dcmFile);
    % Modify the Series ID (if necessary)
    if length(outOpt.SeriesID)< 1
        % Do nothing
    elseif outOpt.SeriesID(1) == '.'
        % Append to existing series ID
        % Not exactly appending, but rather replacing the last bit.
        % i.e.: suppose input Series ID is '.###'
        % and the existing Series ID is 'a.b.cccc.ddddd.eeee.ffff.gggg.hhh'
        % then new Series ID will be 'a.b.cccc.ddddd.eeee.ffff.gggg.###'
        tmpID = regexprep(dcmHeader.SeriesInstanceUID,'\.[^.]*$','');
        dcmHeader.SeriesInstanceUID = [tmpID outOpt.SeriesID];
    else
        % Overwrite existing series ID
        dcmHeader.SeriesInstanceUID = outOpt.SeriesID;
    end
    dcmHeader.SeriesDescription = outOpt.Description;
    dcmHeader.SeriesNumber = outOpt.SeriesNumber;
    % Export the image that matches instance number in dicom header
    curInstance = dcmHeader.InstanceNumber;
    curData = inData(:,:,curInstance);
    outFile = fullfile(outDir,[outOpt.Name '-' num2str(curInstance) '.dcm']);
    dicomwrite(curData,outFile,dcmHeader);
end
%% Done
disp(['Dicom file(s) successfully exported to ' outDir])
end