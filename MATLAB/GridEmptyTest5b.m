clear all
close all
clc

tic
% aiming for both interfaces with 0.8 mm ST
%% Trying to get intersection of all 3 axis
[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\ST0.8');
clear metaD
CT(:,:,664:end)=[];
CT(:,:,1:32)=[];
CThold=CT(:,:,156:313);
CT=CThold;
% CT(1:155,:,:)=0;
% CT(:,1:95,:)=0;
% CT(:,400:end,:)=0;
% CT(350:end,:,:)=0;
CT=mat2gray(CT);
% imshow3D(CT)
CT2=CT;
for i=1:size(CT,3) 
    CT(:,:,i)=imsharpen(CT(:,:,i));
end
% CT(CT<0.05)=0;
CThold2=CT;
% 
% % figure
% % imshow3D(CT)
% 
CT=reshape(CT,512,[]);
CT=imadjust(CT);
CT=reshape(CT, 512, 512, []);
for i=1:size(CT,3) 
    CT(:,:,i)=imsharpen(CT(:,:,i));
end

CT(CT<0.07)=0;


figure
imshow3D(CT)


%% Thresholding
CTthresh=zeros(size(CT));

for i=1:size(CT,3)  
    CTslice=adaptivethreshold(CT(:,:,i),20,0.01,0);
%     CTslice(1:155,:,:)=0;
%     CTslice(:,1:95,:)=0;
%     CTslice(:,400:end,:)=0;
%     CTslice(350:end,:,:)=0;
    CTthresh(:,:,i)=CTslice;

end

%% Remove the white space caused by the thresholding
h= [1 1 1; 1 1 1; 1 1 1];

blobs=imerode(CTthresh,h);
blobs=imerode(blobs,h);
blobs=imdilate(blobs,h);
blobs=imdilate(blobs,h);
for i=1:size(blobs,3)
    blobs(:,:,i)=bwareaopen(blobs(:,:,i),19);
end

% figure
% imshow3D(blobs)
%% Remove the blobs 

CTthresh=CTthresh-blobs;
% figure
% imshow3D(CTthresh)
for i=1:size(CTthresh,3)
    CTthresh(:,:,i)=bwareaopen(CTthresh(:,:,i),20);
%     CTthresh(:,:,i)=bwmorph(CTthresh(:,:,i),'skel',Inf);
end

CTthresh(:,1:110,:)=0;
CTthresh(1:215,:,:)=0;
CTthresh(:,417:end,:)=0;
CTthresh(385:end,:,:)=0;
CTthresh2=CTthresh;

figure
imshow3D(CTthresh)

%% Sobel in 3D or other edge detection
% 
% % maskz(:,:,1)=[1 2 1; 2 4 2; 1 2 1];
% % maskz(:,:,2)=[0 0 0; 0 0 0 ; 0 0 0];
% % maskz(:,:,3)=[-1 -2 -1; -2 -4 -2; -1 -2 -1];
% 
% [gx,gy,gz]=imgradientxyz(CTthresh,'prewitt');
% figure
% edge1=sqrt(gx.^2+gy.^2+gz.^2);
% imshow3D(edge1)
% 
% figure
% edge2=sqrt(gz.^2);
% imshow3D(edge2)


% for i=1:size(CTthresh,3)
%     CTthresh(:,:,i)=bwmorph(CTthresh(:,:,i),'skel',Inf);
% end
% figure
% imshow3D(CTthresh)

% h2(:,:,1)=[0 0 0 ;  0 1 0 ; 0 0 0];
% h2(:,:,2)=[0 0 0 ;  0 1 0 ; 0 0 0];
% h2(:,:,3)=[0 0 0 ;  0 1 0 ; 0 0 0];
% 
% finalImInside=imerode(CTthresh,h2);
% CTthresh=CTthresh-finalImInside;
% figure
% imshow3D(CTthresh);

%% IntLoc
h=[1 1 1;1 1 1; 1 1 1];
CTthresh=imdilate(CTthresh,h);
% figure
% imshow3D(CTthresh)

h=[0 1 0;1 1 1; 0 1 0];
CTthresh=imerode(CTthresh,h);
% figure
% imshow3D(CTthresh)

%% 3D erosion test
% clear h
% h(:,:,1)=[0 0 1 0 0; 0 0 1 0 0;1 1 1 1 1; 0 0 1 0 0; 0 0 1 0 0];
% h(:,:,2)=[0 0 0 0 0; 0 0 0 0 0;0 0 1 0 0; 0 0 0 0 0; 0 0 0 0 0];
% h(:,:,3)=[0 0 1 0 0; 0 0 1 0 0;1 1 1 1 1; 0 0 1 0 0; 0 0 1 0 0];
% 
% finalImInside=imerode(CTthresh,h);
% CTthresh=CTthresh-finalImInside;
% figure
% imshow3D(CTthresh)

% h=[0 0 1 0 0; 0 0 1 0 0;1 1 1 1 1; 0 0 1 0 0; 0 0 1 0 0];
% CTthresh=imopen(CTthresh,h);
% figure
% imshow3D(CTthresh)
% finalIm=CTthresh;



finalIm=zeros(size(CTthresh));
for i=1:size(CT,3)
    finalIm(:,:,i)=CTthresh(:,:,i);
    finalIm(:,:,i)=IntLoc(finalIm(:,:,i));
    finalIm(finalIm>1)=1;
%     finalIm(:,:,i)=IntLoc(finalIm(:,:,i));
%     finalIm(:,:,i)=bwareaopen(finalIm(:,:,i),4);
% %     finalIm=cat(3,finalIm,finalImSlice);
end

CTthresh=finalIm;

% h2(:,:,1)=[0 1 0 ;  1 1 1 ; 0 1 0];
% h2(:,:,2)=[0 0 0 ;  0 1 0 ; 0 0 0];
% h2(:,:,3)=[0 1 0 ;  1 1 1 ; 0 1 0];
% % h2(:,:,1)=[0 0 1 0 0; 0 0 1 0 0;1 1 1 1 1; 0 0 1 0 0; 0 0 1 0 0];
% % h2(:,:,2)=[0 0 0 0 0; 0 0 0 0 0;0 0 1 0 0; 0 0 0 0 0; 0 0 0 0 0];
% % h2(:,:,3)=[0 0 1 0 0; 0 0 1 0 0;1 1 1 1 1; 0 0 1 0 0; 0 0 1 0 0];
% 
% finalImInside=imerode(CTthresh,h2);
% CTthresh=CTthresh-finalImInside;

figure
imshow3D(CTthresh)
% 
% for i=1:size(CT,3)
% 
%     finalIm(:,:,i)=IntLoc(CTthresh(:,:,i));
% %     finalIm(:,:,i)=bwareaopen(finalIm(:,:,i),4);
% % %     finalIm=cat(3,finalIm,finalImSlice);
% end


figure
imshow3D(finalIm)
% imshow3D(CTthresh)

% h=[0 1 1 0; 1 1 1 1; 1 1 1 1; 0 1 1 0];
% test=imopen(finalIm,h);
% figure
% imshow3D(test)


% 
% %% Remove some artifacts again
% arti=zeros(size(CT));
% for i=1:size(CT,3)
%     arti(:,:,i)=bwareaopen(finalIm(:,:,i),15);
%     finalIm(:,:,i)=finalIm(:,:,i)-arti(:,:,i);
% end
% % finalIm=bwareaopen(finalIm,6,6);
% % 
% finalIm(:,:,195:end)=[];
% finalIm(:,:,1:14)=[];
% figure
% imshow3D(finalIm)

% finalIm(:,:,170)=zeros(512,512);
% finalIm(:,:,171)=zeros(512,512);
% imshow3D(finalIm)
% % 
% % %% 3D erosion and subtraction
% % % h(:,:,1)=[0 0 0; 0 1 0; 0 0 0];
% % % h(:,:,2)=[0 0 0; 1 1 1; 1 1 1];
% % % h(:,:,3)=[0 0 0; 1 1 1; 1 1 1];
% % % 
% % % finalIm2=imerode(finalIm,h);
% % % % finalIm=finalIm-finalIm2;
% % % 
% % % imshow3D(finalIm2)
% % 

%% location of the intersection
% L=bwlabeln(finalIm,4);
% c=regionprops(L,'centroid');
% coord1=vertcat(c.Centroid);
% figure
% scatter3(coord1(:,1), coord1(:,2), coord1(:,3), '.');

L=bwconncomp(finalIm,6);
c=regionprops(L,'centroid');
coord1=vertcat(c.Centroid);
figure
scatter3(coord1(:,1), coord1(:,2), coord1(:,3), '.');

toc