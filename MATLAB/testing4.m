clear
close all
clc

%% used to show the linear registration from mimvista is okay 

% [metaD1,CT1]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\Edited0.8');
[metaD1,CT1]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\ST0.8');
% [metaD2,CT2]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\MimRegistration\2016-11__Studies\3mmTo0.8mm');
[metaD2,CT2]=loadDICOM4('C:\Users\Mithunan\Google Drive\Thesis_Grid\MimRegistration\1.5mmTo0.8mm\2016-11__Studies\CT1.5mmTo0.8mmReExport');


imshow3D(CT1)
figure
imshow3D(CT2)
figure
imshowpair(CT1(:,:,86), CT2(:,:,616))
figure
imshowpair(CT1(:,:,102), CT2(:,:,600))
figure
imshowpair(CT1(:,:,587), CT2(:,:,110))


figure
imshow3D(newCT)
