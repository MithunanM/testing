function num=pointCounter(coord)


%% Sorts grid points of every slice
col3= coord(:,3);
change=diff(col3);
steps=find(change>3);
num=diff(steps);



end