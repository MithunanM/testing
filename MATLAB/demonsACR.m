%% 3D demons registration attempt

[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\ACR_CT');
CT(1:2:end,:,:)=[];
CT(:,1:2:end,:)=[];
CT=CT(:,:,78:231);
CT=CT(:,:,1:2:end);
% CT=cat(3, CT(:,:,249), CT(:,:,262), CT(:,:,10), CT(:,:,16), CT(:,:,38), CT(:,:,56), CT(:,:,72), CT(:,:,84), CT(:,:,100), CT(:,:,118), CT(:,:,130));

X=dicomread('T1_SE_X.dcm');

MRI=cat(3,X(:,:,:,1),X(:,:,:,2),X(:,:,:,3),X(:,:,:,4),X(:,:,:,5),X(:,:,:,6),X(:,:,:,7),X(:,:,:,8),X(:,:,:,9),X(:,:,:,10),X(:,:,:,11));
MRI=im2double(MRI);
MRI=imrotate(MRI,270);

[Ireg,Bx,By,Bz,Fx,Fy,Fz] = register_volumes(MRI,CT);