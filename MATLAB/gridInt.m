close all
clear all
clc

%% finding grid intersection points

[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\ACR_CT');

CT=CT(:,:,132);%isolate grid
% CT(CT<1075)=0;%thresh hold
% CT(CT>=1075)=1;
% CT=CT(1:2:end,:,:); %make CT image same size as MRI
% CT=CT(:,1:2:end,:);
CT=mat2gray(CT);
CT=imsharpen(CT);
CT(CT<0.9)=0;%thresh hold
CT(CT>=0.9)=1;

% CT=imcomplement(CT);
% for i=1:2
%     thresh=graythresh(CT);
%     CT(CT<thresh)=0;
% end
% CTholder=CT; 
CC=bwconncomp(CT); % next few lines used to remove circle that outlines grid
numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest,idx] = max(numPixels);
CT(CC.PixelIdxList{idx}) = 0;
% CT=CTholder-CT; %remove the identified circle
imshow(CT,[])


Y=dicomread('T1_SE_Y.dcm');
MRIY=Y(:,:,:,5);
MRIY=im2double(MRIY);
MRIY=mat2gray(MRIY);
MRIY2=imcomplement(MRIY);%invert image to make grid bright
MRIY2=imclearborder(MRIY2); %get rid of the white space around phantom
MRIY2=imsharpen(MRIY2);
thresh=graythresh(MRIY2);
% MRIY2(MRIY2<thresh)=0;
% MRIY2(MRIY2~=0)=1;
MRIY2(MRIY2<0.1)=0;
MRIY2(MRIY2~=0)=1;
figure
imshow(MRIY2)

%% Get grid intersections from function
gridY=IntLoc(MRIY2);

% figure
% imshow(filtInX)
% figure
% imshow(filtInY)
figure
imshow(gridY)
lines=bwareaopen(gridY,5);
gridY=gridY-2*double(lines);
gridY(gridY<1)=0;
figure
imshow(gridY)

coord=findCent(gridY);
figure
imshow(MRIY); title('Manual Threshold')
hold on
plot(coord(:,1),coord(:,2),'r.')
