close all
clear all
clc

% Best 2D registration so far, shows the registered bw grid
%% remove everything but the grid before registration

[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\ACR_CT');
CT=CT(:,:,132);%isolate grid
CT(CT<1075)=0;%thresh hold
CT(CT>=1075)=1;
CT=CT(1:2:end,:,:); %make CT image same size as MRI
CT=CT(:,1:2:end,:);
CT=mat2gray(CT);
CTholder=CT; 
CC=bwconncomp(CT); % next few lines used to remove circle that outlines grid
numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest,idx] = max(numPixels);
CT(CC.PixelIdxList{idx}) = 0;
CT=CTholder-CT; %remove the identified circle
imshow(CT,[]); title('CT');


Y=dicomread('T1_SE_Y.dcm');
MRIY=Y(:,:,:,5);
MRIY=im2double(MRIY);
MRIY=mat2gray(MRIY);
MRIY2=imcomplement(MRIY);%invert image to make grid bright
MRIY2=imclearborder(MRIY2); %get rid of the white space around phantom
MRIY2(MRIY2<0.1)=0;%threshold for visibility
MRIY2(MRIY2>=0.1)=1;
figure
imshow(MRIY2);title('MRIY')

% Z=dicomread('T1_SE_Z.dcm');
% MRIZ=Z(:,:,:,5);
% MRIZ=im2double(MRIZ);
% MRIZ=mat2gray(MRIZ);
% MRIZ2=imcomplement(MRIZ);%invert image to make grid bright
% MRIZ2=imclearborder(MRIZ2); %get rid of the white space around phantom
% MRIZ2(MRIZ2<0.1)=0;%threshold for visibility
% MRIZ2(MRIZ2>=0.1)=1;
% figure
% imshow(MRIZ2)

X=dicomread('T1_SE_X.dcm');
MRIX=X(:,:,:,5);
MRIX=im2double(MRIX);
MRIX=mat2gray(MRIX);
MRIX2=imcomplement(MRIX);%invert image to make grid bright
MRIX2=imclearborder(MRIX2); %get rid of the white space around phantom
MRIX2(MRIX2<0.1)=0;%threshold for visibility
MRIX2(MRIX2>=0.1)=1;
MRIX2=imrotate(MRIX2,270);
figure
imshow(MRIX2);title('MRIX')





%% multimodality demons

[IregX,BxX,ByX,FxX,FyX] = register_images(MRIX2,CT);
[IregY,BxY,ByY,FxY,FyY] = register_images(MRIY2,CT);
% [IregZ,BxZ,ByZ,FxZ,FyZ] = register_images(MRIZ2,CT);

figure
subplot(1,2,1); imshow(IregX,[]); title('X')
subplot(1,2,2); imshow(IregY,[]); title('Y')

figure
imshow(cbimage(CT,IregY)); title('Y')
figure
imshow(cbimage(CT,IregX)); title('X')

figure
colormap gray
subplot(2,2,1); imshow(BxX,[]); title('BxX')
subplot(2,2,2); imshow(ByX,[]); title('BxY')
subplot(2,2,3); imshow(BxY,[]); title('ByX')
subplot(2,2,4); imshow(ByY,[]); title('ByY')


%% get pixel movement of grid
Xind=ones(size(MRIX2));
Xind=Xind.*MRIX2;
DispxX=BxX.*Xind;
DispyX=ByX.*Xind;

Yind=ones(size(MRIY2));
Yind=Yind.*MRIY2;
DispxY=BxY.*Yind;
DispyY=ByY.*Yind;

totDispX=(DispxX.^2 + DispyX.^2).^(1/2);
totDispY=(DispxY.^2 + DispyY.^2).^(1/2);

figure
colormap gray
subplot(2,2,1); imshow(DispxX,[]); title('DispxX')
subplot(2,2,2); imshow(DispyX,[]); title('DispyX')
subplot(2,2,3); imshow(DispxY,[]); title('DispxY')
subplot(2,2,4); imshow(DispyY,[]); title('DispyY')

%% Imregdemons
% % imregdemons doesnt work well
% [dX,moving_regX]=imregdemons(MRIX2,CT);
% [dY,moving_regY]=imregdemons(MRIY2,CT);
% 
% %imregdemons
% figure
% imshow(cbimage(CT,moving_regX)); title('Y')
% figure
% imshow(cbimage(CT,moving_regY)); title('X')
% 
% %again imregdemons is bad
% figure
% colormap gray
% subplot(2,2,1); imshow(dX(:,:,1),[]); title('BxX')
% subplot(2,2,2); imshow(dX(:,:,2),[]); title('BxY')
% subplot(2,2,3); imshow(dY(:,:,1),[]); title('ByX')
% subplot(2,2,4); imshow(dY(:,:,2),[]); title('ByY')
% 


%% For update power point
% close all
% figure
% imagesc(DispxX); title('Displacement on x axis of MRIX');
% colormap gray
% figure
% imagesc(DispyX); title('Displacement on y axis of MRIX');
% colormap gray
% figure
% imagesc(DispxY); title('Displacement on x axis of MRIY');
% colormap gray
% figure
% imagesc(DispyY); title('Displacement on y axis of MRIY');
% colormap gray
% 
% figure
% imagesc(totDispX); title('Total displacement X');
% colormap gray
% 
% figure
% imagesc(totDispY); title('Total displacement Y');
% colormap gray
