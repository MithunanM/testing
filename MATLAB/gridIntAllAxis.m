close all
clear all
clc

% only finding the grid intersection points on an image
%% finding grid intersection points and display the dots

[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\ACR_CT');

CT=CT(:,:,132);%isolate grid
CT=mat2gray(CT);
CT=imsharpen(CT);
% CT(1:2:end,:,:)=[];
% CT(:,1:2:end,:)=[];
CT(CT<0.9)=0;%thresh hold
CT(CT>=0.9)=1;

CC=bwconncomp(CT); % next few lines used to remove circle that outlines grid
numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest,idx] = max(numPixels);
CT(CC.PixelIdxList{idx}) = 0;
imshow(CT,[]); title('CT');
CTtest=CT;

Y=dicomread('T1_SE_Y.dcm'); %load and threshold image, MRIY2 is thresholded image image
MRIY=Y(:,:,:,5);
y=MRIY;
MRIY2=thresh(MRIY);
figure
imshow(MRIY2); title('Y')

X=dicomread('T1_SE_X.dcm');
MRIX=X(:,:,:,5);
MRIX2=thresh(MRIX,270);
figure
imshow(MRIX2); title('X')

%% Get grid intersections from function

CT=medfilt2(CT); %filter a little to make it look better
se=strel('line',2,90);
CT=imclose(CT,se);
se=strel('line',2,0);
CT=imclose(CT,se);

gridCT=IntLoc(CT,3,3); %get the intersection of the grid
se=strel('disk',1);
gridCT=imopen(gridCT,se);

CTcent=findCent(gridCT); % get pixel location of the centers

figure
imshow(gridCT)
title('CT')
hold on
plot(CTcent(:,1), CTcent(:,2), 'b*');
hold off

% MRIY2=medfilt2(MRIY2);
gridY=IntLoc(MRIY2);

MRIY2cent=findCent(gridY,4); % get pixel location of the centers

figure
imshow(gridY)
title('Y')
hold on
plot(MRIY2cent(:,1), MRIY2cent(:,2), 'r*');
hold off

% MRIX2=medfilt2(MRIX2);
se=strel('line',2,90);
MRIX2=imclose(MRIX2,se);
se=strel('line',2,0);
MRIX2=imclose(MRIX2,se);
gridX=IntLoc(MRIX2);

MRIX2cent=findCent(gridX,4); % get pixel location of the centers

figure
imshow(gridX)
title('X')
hold on
plot(MRIX2cent(:,1), MRIX2cent(:,2), 'r*');
hold off


% Xind=ones(size(CT));
% Xind=Xind.*MRIX2;
% DispxX=BxX.*Xind;
% DispyX=ByX.*Xind;

%% opening for intersections and other random tests
% 
% se=strel('disk',1);
% gridCT=imopen(CT,se);
% h=fspecial('average', [2 2]);
% gridCT=filter2(h,gridCT);
% gridCT(gridCT<.99)=0;
% figure
% imshow(gridCT)
% 
% close all
% 
% figure
% imshow(CTtest,[]); title('CT')
% hold on
% plot(CTcent(:,1),CTcent(:,2),'b*')
% 
% figure
% x=imrotate(MRIX,-90);
% imshow(x,[]); title ('X')
% hold on
% plot(MRIX2cent(:,1), MRIX2cent(:,2), 'r*');
% hold off

% figure
% imshow(y,[]);title('Y');
% hold on
% plot(MRIY2cent(:,1), MRIY2cent(:,2), 'r*');
% hold off
% 
%% difference in pixel location

