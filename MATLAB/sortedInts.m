function full3D=sortedInts(coords)
%% Sorting the coordinates of the intersections
col3= coords(:,3);
change=diff(col3);
steps=find(change>3);
steps=[0 ; steps];
full3D=zeros(size(coords));

for j=1:size(steps,1)
    if j==size(steps,1)
        slice=coords(steps(j)+1:end,:);
        change2=diff(slice(:,1));%starting here sorts the grid intersection of 1 slice
        stepsInside=find(change2>3);
        stepsInside=[0 ; stepsInside];
        finalSteps=zeros(size(slice));
        for i=1:size(stepsInside,1) 
    
            if i==size(stepsInside,1)
                holdSteps=slice(stepsInside(i)+1:end,:); %deal with final set of intersections
                holdSteps=sortrows(holdSteps,2);
                finalSteps(stepsInside(i)+1:end,:)=holdSteps;
            else %sort the intersections
                holdSteps=slice(stepsInside(i)+1:stepsInside(i+1),:);
                holdSteps=sortrows(holdSteps,2);
                finalSteps(stepsInside(i)+1:stepsInside(i+1),:)=holdSteps;
            end
        end
        
        full3D(steps(j)+1:end,:)=finalSteps;
    else
        slice=coords(steps(j)+1:steps(j+1),:);
        change2=diff(slice(:,1));%starting here sorts the grid intersection of 1 slice
        stepsInside=find(change2>3);
        stepsInside=[0 ; stepsInside];
        finalSteps=zeros(size(slice));
        for i=1:size(stepsInside,1) 
    
            if i==size(stepsInside,1)
                holdSteps=slice(stepsInside(i)+1:end,:); %deal with final set of intersections
                holdSteps=sortrows(holdSteps,2);
                finalSteps(stepsInside(i)+1:end,:)=holdSteps;
            else %sort the intersections
                holdSteps=slice(stepsInside(i)+1:stepsInside(i+1),:);
                holdSteps=sortrows(holdSteps,2);
                finalSteps(stepsInside(i)+1:stepsInside(i+1),:)=holdSteps;
            end
        end
        full3D(steps(j)+1:steps(j+1),:)=finalSteps;
    end


end


end