function image=makeSameSize(im1,im2)

s1=size(im1);
s2=size(im2);
d1=abs(s1(1)-s2(1));
d2=abs(s1(2)-s2(2));
odd1=mod(d1,2);
odd2=mod(d2,2);

if (s1(1)<s2(1))
    image=padarray(im1, [floor(d1/2) floor(d2/2)]);
else
    image=padarray(im2, [floor(d1/2) floor(d2/2)]);
end

if odd2==1
    image= [zeros(size(image,1),1), image];
end
if odd1==1    
    image= [zeros(1,size(image,2)); image];
end







end