function scatdisplacement=scatdiff(s1,s2,size)
if (nargin==2)
    size=1;
end
%% first scatter plot, original image centers

firstCol=s1(:,1);
x=diff(firstCol);
step=find(x>10);
colsO=zeros(12,2,11);

for i=1:11
    if i==1
        colsO(1:step(i),:,i)=sortrows(s1(1:step(i),:),2);
    elseif i==11
        colsO(1:(size(s1,1)-step(end)),:,i)=sortrows(s1(step(i-1)+1:end,:),2);
    else
        
        colsO(1:(step(i)-step(i-1)),:,i)=sortrows(s1(step(i-1)+1:step(i),:),2);
        
    end
end


%% second scatter plot, registered image

firstCol=s2(:,1);
x=diff(firstCol);
step=find(x>10);
colsR=zeros(12,2,11);

for i=1:11
    if i==1
        colsR(1:step(i),:,i)=sortrows(s2(1:step(i),:),2);
    elseif i==11
        colsR(1:(size(s1,1)-step(end)),:,i)=sortrows(s2(step(i-1)+1:end,:),2);
    else
        
        colsR(1:(step(i)-step(i-1)),:,i)=sortrows(s2(step(i-1)+1:step(i),:),2);
        
    end
end

scatdisplacement=colsO-colsR.*size;
end