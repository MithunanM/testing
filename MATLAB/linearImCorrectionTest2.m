close all
clear all
clc

%Linear registers the ACR phantom and then uses the demons to get the
%displacement.
%% Linear Registration followed by Demons

[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\ACR_CT');
CT=CT(:,:,132);%isolate grid
CTorigin=CT;
CTorigin=im2double(CTorigin);
CTorigin=mat2gray(CTorigin);
CT(CT<1075)=0;%thresh hold
CT(CT>=1075)=1;
% CT=CT(1:2:end,:,:); %make CT image same size as MRI
% CT=CT(:,1:2:end,:);
CT=mat2gray(CT);
CTholder=CT; 
CC=bwconncomp(CT); % next few lines used to remove circle that outlines grid
numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest,idx] = max(numPixels);
CT(CC.PixelIdxList{idx}) = 0;
CT=CTholder-CT; %remove the identified circle
imshow(CT,[]); title('CT');


X=dicomread('T1_SE_X.dcm');
MRIX=X(:,:,:,5);
MRIX=im2double(MRIX);
MRIX=mat2gray(MRIX);
MRIX2=imcomplement(MRIX);%invert image to make grid bright
MRIX2=imclearborder(MRIX2); %get rid of the white space around phantom
MRIX2(MRIX2<0.1)=0;%threshold for visibility
MRIX2(MRIX2>=0.1)=1;
MRIX2=imrotate(MRIX2,270);
% figure;imshow(MRIX2); title('MRIX');

%% The X registrations 
[optimizer,metric]=imregconfig('monomodal');
[linRegdImX]=imregister(MRIX2,CT,'affine',optimizer,metric);

figure;imshow(linRegdImX); title('Linear Registration');
figure;imshow(cbimage(linRegdImX,CT)); title('Both images after linear registration')

[IregX,BxX,ByX,FxX,FyX]=register_images(linRegdImX,CT);
% figure;imagesc(BxX); title('BxX')
% figure;imagesc(ByX); title('ByX')

figure; imshow(cbimage(IregX,CT)); title('nonlinear kroons cb')
figure; imshow(IregX); title('Demons kroons')


Xind=ones(size(linRegdImX));
Xind=Xind.*linRegdImX;
DispxX=BxX.*Xind;
DispyX=ByX.*Xind;

figure; imagesc(DispxX); title('Displacement in y of MRIX'); colormap gray;
figure; imagesc(DispyX); title('Displacement in x of MRIX'); colormap gray;
totDispX=(DispxX.^2 + DispyX.^2).^(1/2);
figure; imagesc(totDispX); title('Total Displacement'); colormap gray;

figure
imshow(CTorigin); title('CT Image');
figure
showMRI=imrotate(MRIX,270);
imshow(showMRI);title('MRI Image')

%% The Y registrations
% 
% Y=dicomread('T1_SE_Y.dcm');
% MRIY=Y(:,:,:,5);
% MRIY=im2double(MRIY);
% MRIY=mat2gray(MRIY);
% MRIY2=imcomplement(MRIY);%invert image to make grid bright
% MRIY2=imclearborder(MRIY2); %get rid of the white space around phantom
% MRIY2(MRIY2<0.1)=0;%threshold for visibility
% MRIY2(MRIY2>=0.1)=1;
% figure;imshow(MRIY2); title('MRIY')
% 
% [optimizer,metric]=imregconfig('monomodal');
% [linRegdImY]=imregister(MRIY2,CT,'affine',optimizer,metric);
% figure;imshow(linRegdImY); title('Linear Registration');
% figure;imshow(cbimage(linRegdImY,CT)); title('Both images')
% 
% [IregY,BxY,ByY,FxY,FyY]=register_images(linRegdImY,CT);
% 
% Yind=ones(size(linRegdImY));
% Yind=Yind.*linRegdImY;
% DispxY=BxY.*Yind;
% DispyY=ByY.*Yind;
% 
% 
% figure; imagesc(DispxY); title('Displacement in x of MRIY'); colormap gray;
% figure; imagesc(DispyY); title('Displacement in y of MRIY'); colormap gray;
% figure; imshow(cbimage(IregY,CT)); title('Demons registration and CT')
% figure; imshow(IregY); title('Just Demons')
% totDispY=(DispxY.^2 + DispyY.^2).^(1/2);
% totDispYFull=(BxY.^2 + ByY.^2).^(1/2);
% figure; imagesc(totDispYFull); title('Total Displacement of whole image'); colormap gray
% figure; imagesc(totDispY); title('Total Displacement'); colormap gray;