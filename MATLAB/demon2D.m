%% 2D demons registration attempt latest
close all

[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\mri_acr_data\ACR_CT');
CT=CT(:,:,132);
CT=CT(1:2:end,:,:);
CT=CT(:,1:2:end,:);

X=dicomread('T1_SE_X.dcm');
Y=dicomread('T1_SE_Y.dcm');
Z=dicomread('T1_SE_Z.dcm');

MRIX=X(:,:,:,5);
MRIX=im2double(MRIX);
MRIX=imrotate(MRIX,270);

MRIZ=Z(:,:,:,5);
MRIZ=im2double(MRIZ);

MRIY=Y(:,:,:,5);
MRIY=im2double(MRIY);

CT=mat2gray(CT);
MRIX=mat2gray(MRIX);
MRIY=mat2gray(MRIY);
MRIZ=mat2gray(MRIZ);
% CT=histeq(CT);
% [D2,reg_im2]=imregdemons(MRI,CT);
% movingreg=imregister(MRI,CT)
[IregX,BxX,ByX,FxX,FyX] = register_images(MRIX,CT);
[IregY,BxY,ByY,FxY,FyY] = register_images(MRIY,CT);
[IregZ,BxZ,ByZ,FxZ,FyZ] = register_images(MRIZ,CT);


imshow(CT,[]); title('CT');
figure
subplot(3,3,1), imshow(MRIX,[]); title('MRI X');
subplot(3,3,2), imshow(IregX,[]); title('Registered image X');
subplot(3,3,3), imshow(cbimage(MRIX,IregX),[]); title('X and warped X');
subplot(3,3,4), imshow(MRIY,[]); title('MRI Y');
subplot(3,3,5), imshow(IregY,[]); title('Registered image Y');
subplot(3,3,6), imshow(cbimage(MRIY,IregY),[]); title('Y and warped Y');
subplot(3,3,7), imshow(MRIZ,[]); title('MRI Z');
subplot(3,3,8), imshow(IregZ,[]); title('Registered image Z');
subplot(3,3,9), imshow(cbimage(MRIZ,IregZ),[]); title('Z and warped Z');
figure

subplot(2,3,1), imshow(BxX); title('BxX');
subplot(2,3,2), imshow(ByX); title('ByX');
subplot(2,3,3), imshow(BxY); title('BxY');
subplot(2,3,4), imshow(ByY); title('ByY');
subplot(2,3,5), imshow(BxZ); title('BxZ');
subplot(2,3,6), imshow(ByZ); title('ByZ');

figure
subplot(1,3,1), imshow(cbimage(MRIX,IregX),[]); title('X and warped X');
subplot(1,3,2), imshow(cbimage(MRIY,IregY),[]); title('Y and warped Y');
subplot(1,3,3), imshow(cbimage(MRIZ,IregZ),[]); title('Z and warped Z');

figure
subplot(1,3,1), imshow(MRIX-IregX,[]); title('X subtracted');
subplot(1,3,2), imshow(MRIY-IregY,[]); title('Y subtracted');
subplot(1,3,3), imshow(MRIZ-IregZ,[]); title('Z subtracted ');


figure
subplot(1,2,1), imshow(cbimage(imadjust(MRIX),imadjust(CT))); title('X no registration')
subplot(1,2,2), imshow(cbimage(imadjust(IregX),imadjust(CT))); title('X registration')
figure
subplot(1,2,1), imshow(cbimage(imadjust(MRIY),imadjust(CT))); title('Y no registration')
subplot(1,2,2), imshow(cbimage(imadjust(IregY),imadjust(CT))); title('Y registration')
figure
subplot(1,2,1), imshow(cbimage(imadjust(MRIZ),imadjust(CT))); title('Z no registration')
subplot(1,2,2), imshow(cbimage(imadjust(IregZ),imadjust(CT))); title('Z registration')
