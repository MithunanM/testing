close all
clear all
clc

%Finding grid points using adaptive thresholding 
%% finding the grid intersection of the 3D grid phantom
[metaD,CT]=loadDICOM4('C:\Users\Mithunan\Desktop\fullGrid\CT');
CThold=CT;
CTf=[];
CT(1:155,:,:)=0;
CT(:,1:87,:)=0;
CT(:,420:end,:)=0;
CT(360:end,:,:)=0;
for i=1:size(CT,3)
    CTslice=mat2gray(CT(:,:,i));
    CTf=cat(3,CTf,CTslice);
end
CT=CTf;
CT2=CT;
CTthresh=[]; %This will be the thresholded CT image
CTtest=[];

for i=10:size(CT,3)-5
    CTslice=adaptivethreshold(CTf(:,:,i),19,0.01,0);
    CTslice(1:155,:,:)=0;
    CTslice(:,1:87,:)=0;
    CTslice(:,420:end,:)=0;
    CTslice(360:end,:,:)=0; %by here you have the thresholded CT image
    
    h= [ 0 1 0; 1 1 1 ; 0 1 0];
    CTslice=imerode(CTslice,h);
    CTslice=mat2gray(CTslice); % by here you have a skinny grid
    CTslice(:,1:92,:)=0;
    CTslice(:,415:end,:)=0;
    
    CTtestslice=CTslice;
    CTtest=cat(3,CTtest,CTtestslice);
    
    CTslice=IntLoc(CTslice); %intersection
    
    CTthresh=cat(3,CTthresh,CTslice);
    
end
imshow3D(CTthresh)

%% below gets the coordinates 
figure
L=bwlabeln(CTthresh,8);
c=regionprops(L,'centroid');
coord=vertcat(c.Centroid);
scatter3(coord(:,1), coord(:,2), coord(:,3),'.')