close all
clear
clc

%%Testing some simulations from the Helms paper
% Recreates all graphs from the Helms paper

%% Decay of cuadate/splenium and pallidum

Amp0=1;
T1=773;
T2s=[45.3, 45.4,  29] ;
TR=66;
% TE=linspace(0,70,200);
TE=[1:12].*4.92;
alpha= 35 *pi/180;
q=(alpha^2)/(2*TR);

for i=1:size(T2s,2)
    A_TE(:,i)= Amp0*exp(-TE./T2s(:,i));
end


figure
plot(TE,A_TE(:,2),'--s')
hold on
plot(TE,A_TE(:,3),'--d')
xlim([4.92,4.92*12])

% Consecutive averaging 
cumulativeAvg= cumsum(A_TE)./[1:12]';
hold on
plot(TE,cumulativeAvg(:,2),'-s')
plot(TE,cumulativeAvg(:,3),'-d')
ylim([0 2.5])





%% Testing 4 signal equation from the paper, This gets the signal values
% Reseting parameters incase theres a change needed

Amp0=1;
T1=[773, 1277, 970];
T2s=[45.3, 45.4, 29];
TR=66;
% TE=linspace(0,70,200);
TE=[1:12].*4.92;
alpha= 35 *pi/180;
q=(alpha^2)/(2*TR);

% 
% S1= cumulativeAvg.* ((1-exp(-TR./T1))/(1-cos(alpha)*exp(-TR./T1)))*sin(alpha);
% S2= cumulativeAvg.*((TR./T1)./((alpha^2)/2 +TR./T1)) .* alpha;
% S3= (cumulativeAvg*alpha) ./(q.*T1+1);
S4sim3= cumulativeAvg.* (sqrt(2*q*TR)) .* (1./(q.*T1+1));

% S1= A_TE .* ((1-exp(-TR./T1))/(1-cos(alpha)*exp(-TR./T1)))*sin(alpha);
% S2= A_TE.*((TR./T1)./((alpha^2)/2 +TR./T1)) .* alpha;
% S3= (A_TE*alpha) ./(q.*T1+1);
% S4= A_TE.* (sqrt(2*q*TR)) .* (1./(q.*T1+1));
% SNR=A_TE*sqrt(q)/(q.*T1+1);

% figure
% plot(TE,S1(:,1))
% % hold on
% plot(TE,S2(:,1))
% hold on
% plot(TE,S3(:,1))
% plot(TE,S4(:,1))
% 
% plot(TE,S1(:,2))
% plot(TE,S2(:,2))
% plot(TE,S3(:,2))
% plot(TE,S4(:,2))
% 
% plot(TE,S1(:,3))
% plot(TE,S2(:,3))
% plot(TE,S3(:,3))
% plot(TE,S4(:,3))

% legend('1','2','3','4','5','6','7','8','9','10','11','12')
% figure

%% Interpolating SNR Values, this gets some assumed noise values from the plots in the paper
[SNR1,SNR2]=SimsSNRCurves;
% 
% scaledNoise1=S4(:,1)./SNR1;
% scaledNoise2=S4(:,2)./SNR2;
scaledNoise1=S4sim3(:,1)./SNR1;
scaledNoise2=S4sim3(:,3)./SNR2;


% figure
% plot([1:12]*4.92,scaledNoise1)
% hold on
% plot([1:12]*4.92,scaledNoise2)
% xlabel('TE [ms]')
% ylabel('Scaled stdev of noise')
% figure
%% Normalized SNR from the assumed noise, renormalize a second time to first echo

normSNR1 = S4sim3(:,1)./scaledNoise1;
normSNR2 = S4sim3(:,2)./scaledNoise1; % Differently scaled signal value that im using 
% normSNR2 = S4(:,3)./scaledNoise2;
normSNR3 = S4sim3(:,3)./scaledNoise1;
normSNR3b = S4sim3(:,3)./scaledNoise2; %This is the normalization from the paper


renormSNR1 = normSNR1/normSNR1(1);
renormSNR2 = normSNR2/normSNR2(1);
renormSNR3 = normSNR3/normSNR3(1);

% TE(:,13:end)=[];
% normSNR1(13:end,:)=[];
% normSNR2(13:end,:)=[];


%% Up to here plots the oringinal graph from the paper
plot(TE,normSNR1,'-s');
plot(TE,normSNR3b,'-d');
xlabel('TE [ms]')
ylabel('Signal [a.u.] / "Normalized" SNR')
legend('caudate/splenium', 'pallidum', 'consecutively averaged C/S',...
    'consecutively averaged P', 'Normalized SNR C/S', 'Normalized SNR P', ...
    'Location', 'east')
grid on

%% Reploting because original graph is ugly, plus new plot
% Shows the cumulative T2* average signal
figure
plot(TE,A_TE(:,2),'--s')
hold on
plot(TE,A_TE(:,3),'--d')
plot(TE,cumulativeAvg(:,2),'-s')
plot(TE,cumulativeAvg(:,3),'-d')
ylim([0 1.25])
xlim([4.92,4.92*12])
legend('caudate/splenium', 'pallidum', 'consecutively averaged C/S', 'consecutively averaged P')
xlabel('TE [ms]')
ylabel('Signal [a.u.]')

%The three signals 
figure
plot(TE,S4sim3(:,1));
hold on
plot(TE,S4sim3(:,2));
plot(TE,S4sim3(:,3));
xlabel('TE [ms]')
ylabel('Signal [a.u.]')

legend ('caudate', 'splenium', 'pallidum')

%Shows the renormalized SNR values 
figure
plot(TE,normSNR1,'-s');
hold on
% plot(TE,normSNR2,'-d');
plot(TE,normSNR3b,'-s')


plot(TE,renormSNR1,'-*')
plot(TE,renormSNR2,'-*');
plot(TE,renormSNR3,'-*')
xlim([4.92,4.92*12])
xlabel('TE [ms]')
ylabel('Normalized SNR')
title('Scaled noise 1 used to get SNR then renormalized')

% legend('Normalized SNR Splenium', 'Normalized SNR Caudate', 'Normalized SNR Pallidum',...
%      'renormalized SNR Splenium', 'renormalized SNR Caudate', 'renormalized SNR Pallidum' ,'Location', 'southeast')

legend('Normalized SNR S/C from paper', 'Normalized SNR Pallidum from paper',...
     'renormalized SNR Splenium', 'renormalized SNR Caudate', 'renormalized SNR Pallidum' ,'Location', 'southeast')

 
%% Step by step plotting for the SNR renormalization

figure
plot(TE,normSNR1,'-s');
hold on
plot(TE,normSNR2,'-d');
plot(TE,normSNR3,'-*');
title('SNR calculated using scaled noise')
xlabel('TE [ms]')
ylabel('SNR from scaled noise')
legend('Splenium','Caudate','Pallidum')

%% Sim 2, looking at root TR dependence in the signal

TR=linspace(5,65,200);
T1=[773, 1277, 970];
q=2.83;

for i=1:size(T1,2)
    S4sim2(:,i)= 1 * (sqrt(2*q*TR)) .* 1/(q*T1(i)+1);
end

figure
plot(TR,S4sim2)
legend('Splenium', 'Caudate', 'Pallidum')

xlabel('TR [ms]')
ylabel('Signal [a.u.]')



%% Sim 3, adapting the TR to the ETL 
% close all

TR=66;
TRn= TE' +7;
% qsim3=alpha.* TR;
S4sim3= cumulativeAvg.* (sqrt(2*q.*TRn)) .* (1./(q.*T1+1)); % cumulative signal

% figure
% plot(TE,S4sim3)
% S4sim3=S4sim3./sqrt(1:12)';

S4sim3(:,1)=S4sim3(:,1)./S4sim3(1,1); % cumulative signal normalized
S4sim3(:,2)=S4sim3(:,2)./S4sim3(1,2);
S4sim3(:,3)=S4sim3(:,3)./S4sim3(1,3);

SNR1sim3= S4sim3(:,1)./scaledNoise1; %renormalized SNR values 
SNR1sim3= SNR1sim3/SNR1sim3(1);

SNR2sim3= S4sim3(:,2)./scaledNoise1;
SNR2sim3= SNR2sim3/SNR1sim3(1);

SNR3sim3= S4sim3(:,3)./scaledNoise1;
SNR3sim3= SNR3sim3/SNR3sim3(1);
SNRsim3= [SNR1sim3 SNR2sim3 SNR3sim3]; 

SNReffsim3=SNRsim3./(sqrt(TRn));
SNReffsim3(:,1)=SNReffsim3(:,1)./SNReffsim3(1,1);
SNReffsim3(:,2)=SNReffsim3(:,2)./SNReffsim3(1,2);
SNReffsim3(:,3)=SNReffsim3(:,3)./SNReffsim3(1,3);


% SNR1

figure
plot(TRn,S4sim3, '--')
hold on
% plot(TRn,SNR1sim3)
% plot(TRn,SNR2sim3)
% plot(TRn,SNR3sim3)
plot(TRn,SNRsim3, '-')
plot(TRn,SNReffsim3)

ylim([0 5])
xlabel('TR [ms]')
ylabel('Signal [a.u.]/ Normalized SNR')

legend('avg signal S', 'avg signal C', 'avg signal P', ...
    'Renormalized SNR S', 'Renormalized SNR C', 'Renormalized SNR P',...
    'SNR/sqrt(TRn) S', 'SNR/sqrt(TRn) C', 'SNR/sqrt(TRn) P', 'Location', 'northwest')

grid on

%% Test the SNR eff with the first sim to see if theyre the same 

differ= [renormSNR1 renormSNR2 renormSNR3] - SNReffsim3;

figure
plot(TE, differ)
ylabel('Difference in sim1 and sim3 SNR eff')
xlabel('TE [ms]')
legend('Splenium', 'Caudate', 'Pallidum') 
